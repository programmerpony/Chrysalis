[![](https://chrysalis.programmerpony.com/images/preview.png)](https://chrysalis.programmerpony.com/)

[![Discord Bots](https://top.gg/api/widget/status/797161820594634762.svg?noavatar=true)](https://discord.gg/Vj2jYQKaJP)

# What's Chrysalis?

Chrysalis is a general purpose, completely free and open source, MLP-themed Discord bot. I made this bot because I'm sick of proprietary bots that charge you for half of their features, and you probably are too. [Try Chrysalis today!](https://discord.com/api/oauth2/authorize?client_id=797161820594634762&permissions=8&scope=bot%20applications.commands)

# How to contribute

Any contribution is welcome! From fixing typos and adding GIFs to adding new languages and commands.  

You can also help translating the bot. Chrysalis is currently available in English, Spanish, French and Italian. Pull requests fixing typos are welcome too.  

With your help, we can make Chrysalis the only bot you'll ever need!

# How to build

###### Chrysalis requires Node.js v20.15.0 or higher to run.

1. Fork and clone the repository.
2. Run `npm i` to install the dependencies.
3. Install MongoDB on your server.
4. Make a copy of `.env_sample`, rename it to `.env` and replace the corresponding values in it.
5. Follow the instructions on `src/emojis.js` to make custom emojis work.
6. Start the bot using `npm run dev` (for development) or `npm start` (for production).

# License

```
Copyright (C) 2022-2024 programmerpony

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

# Support

Do you need help with the bot? Join our [support server](https://discord.gg/Vj2jYQKaJP).
