/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { ApplicationCommandOptionType } from 'discord.js';
import defaultModules from '../defaultModules.js';
import { NitroBoost, PinkiePieYes } from '../emojis.js';

export const meta = {
  name: 'English',
  new_lang_message: `I'll speak in English now.`
}

/*
DO NOT TRANSLATE COMMAND NAMES, only descriptions, option names and option descriptions
Command description - Max 100 characters
Command option name - Max 32 chracters
Command option description - Max 100 characters
*/
export const commands = {
  user: [
    {
      name: 'avatar',
      description: 'Sends the avatar of a user.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user whose avatar you want to be sent.'
        }
      ]
    },
    {
      name: 'userinfo',
      description: 'Shows info about a user.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user you want to display info of.'
        }
      ]
    },
    {
      name: 'roleinfo',
      description: 'Shows info about a role.',
      options: [
        {
          name: 'role',
          type: ApplicationCommandOptionType.Role,
          description: 'The role to display info of.',
          required: true
        }
      ]
    },
    {
      name: 'serverinfo',
      description: 'Shows info about the server.'
    },
    {
      name: 'rank',
      description: 'Shows your current level and XP.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user to display the rank of.'
        }
      ]
    },
    {
      name: 'leaderboard',
      description: 'Shows the users with the most XP.'
    },
    {
      name: 'profile',
      description: 'Edit your profile (on all servers).'
    },
    {
      name: 'emoji',
      description: 'Sends the emoji as an image.',
      options: [
        {
          name: 'emoji',
          type: ApplicationCommandOptionType.String,
          description: 'The emoji you want to convert to an image.',
          required: true
        }
      ]
    },
    {
      name: 'love',
      description: 'Checks the compatibility level between two users.',
      options: [
        {
          name: 'lover1',
          type: ApplicationCommandOptionType.User,
          description: 'First member of the couple.',
          required: true
        },
        {
          name: 'lover2',
          type: ApplicationCommandOptionType.User,
          description: 'Second member of the couple.',
          required: true
        }
      ]
    },
    {
      name: 'boop',
      description: 'Boops a user.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user you want to boop.',
          required: true
        }
      ]
    },
    {
      name: 'hug',
      description: 'Hugs a user.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user you want to hug.',
          required: true
        }
      ]
    },
    {
      name: 'kiss',
      description: 'Kiss a user.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user you want to kiss.',
          required: true
        }
      ]
    },
    {
      name: 'booru',
      description: 'Sends an image from Manebooru.',
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags to search (separated by commas).'
        }
      ]
    },
    {
      name: 'clop',
      description: 'Sends a clop image from Manebooru.',
      nsfw: true,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags to search (separated by commas).'
        }
      ]
    },
    {
      name: 'e621',
      description: 'Sends an image from e621.',
      nsfw: true,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags to search (separated by spaces).'
        }
      ]
    },
    {
      name: 'torrent',
      description: 'Torrent ponies!'
    },
    {
      name: 'say',
      description: 'Chrysalis says what you type.',
      options: [
        {
          name: 'text',
          type: ApplicationCommandOptionType.String,
          description: 'The text you want Chrysalis to say.',
          required: true
        }
      ]
    }
  ],
  admin: [
    {
      name: 'config',
      description: `Edit Chrysalis' modules`,
      options: [
        {
          name: 'module',
          type: ApplicationCommandOptionType.String,
          description: 'Choose the module that you want to edit.',
          choices: defaultModules.map(m => { return { name: m.name, value: m.name } }),
          required: true
        }
      ]
    },
    {
      name: 'clean',
      description: 'Bulk delete messages.',
      options: [
        {
          name: 'messages',
          type: ApplicationCommandOptionType.Integer,
          description: 'The amount of messages that you want to delete.',
          minValue: 1,
          maxValue: 100,
          required: true
        }
      ]
    },
    {
      name: 'lang',
      description: `Change Chrysalis' language.`,
      options: [
        {
          name: 'language',
          type: ApplicationCommandOptionType.String,
          description: 'The language you want Chrysalis to speak in.',
          choices: [
            {
              name: 'English',
              value: 'en'
            },
            {
              name: 'Spanish',
              value: 'es'
            },
            {
              name: 'Italian',
              value: 'it'
            },
            {
              name: 'French',
              value: 'fr'
            }
          ],
          required: true
        }
      ]
    },
    {
      name: 'color',
      description: 'Change the default color for embeds.',
      options: [
        {
          name: 'color',
          type: ApplicationCommandOptionType.String,
          description: 'Must be a hexadecimal color.',
          maxLength: 7
        }
      ]
    },
    {
      name: 'welcome',
      description: 'Sends a welcome card for the specified user.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user you want to welcome.'
        }
      ]
    },
    {
      name: 'boost',
      description: 'Sends a boost card for the specified user.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user you want to show the boost card for.'
        }
      ]
    },
    {
      name: 'setxp',
      description: 'Set the amount of XP that the specified user has.',
      options: [
        {
          name: 'user',
          type: ApplicationCommandOptionType.User,
          description: 'The user you want to set the XP of.',
          required: true
        },
        {
          name: 'xp',
          type: ApplicationCommandOptionType.Integer,
          description: 'The new amount of XP.',
          minValue: 0,
          maxValue: Number.MAX_SAFE_INTEGER,
          required: true
        }
      ]
    },
    {
      name: 'importxp',
      description: 'Import XP from other level bots!'
    },
    {
      name: 'rolemenu',
      description: 'Set up a menu for users to pick roles from!',
      options: [
        {
          name: 'edit',
          type: ApplicationCommandOptionType.String,
          description: 'If you want to edit an already existing role menu, paste the message URL here.'
        }
      ]
    }
  ]
}
export const defaultValues = {
  bannedwords: {
    message: 'Your message was removed due to containing something not allowed in the server. Please, be careful with what you say.'
  },
  boost: {
    message: '**{user} just boosted the server!**',
    title: 'Thank you for boosting the server!',
    description: `${NitroBoost} Enjoy your exclusive role! ${NitroBoost}`
  },
  welcome: {
    message: 'Welcome to **{guild}**! {user}'
  },
  goodbye: {
    message: 'Goodbye, **{user}**. Hope we can see you again soon.',
    banMessage: '**{user}** was banned from the server.'
  },
  autorole: {
    captchaDescription: 'Solve this captcha to gain access to **{guild}**. You have {seconds} seconds before you get kicked automatically.',
    captchaKickMessage: 'You were kicked from **{guild}** for not solving the captcha.'
  }
}
export const author = 'Author'
export const message = 'Message'
export const channel = 'Channel'
export const user_commands = 'Commands:'
export const admin_commands = 'Admin commands:'
export const wrong_channel = 'The command you sent cannot be used in that channel.'
export const message_deleted = 'Message deleted'
export const you_must_send_an_emoji = 'You need to send an emoji.'
export const couldn_t_find_that_emoji = `Couldn't find that emoji.`
export const type_one_or_two_users = 'Type one or two users.'
export const self_love = `I know self-love is important, but don't be a narcissist.`
export const lovemeter_messages = [
  'Sad.',
  'Incompatible.',
  'Very low... Just friends.',
  'Probably just friends.',
  'Very unlikely but there may be something.',
  `There could be something, but I don't want to get your hopes up.`,
  'Very likely to be something. You can try.',
  'Quite compatible. Could be a cute couple.',
  'These two are making out for sure.',
  'Could be a wonderful couple.',
  'They are meant to be together forever.'
]
export const nsfw_only = 'This command can only be used in NSFW channels.'
export const requested_by = 'Requested by'
export const how_to_delete = 'Click here if this image is inappropriate.'
export const please_report = `Please report this image so it doesn't show up again.`
export const role_info = 'Role info:'
export const name = 'Name'
export const member_count = 'Member count'
export const date_created = 'Date created'
export const couldn_t_find_that_user = `Couldn't find that user.`
export const user_info = 'User info:'
export const user_id = 'User ID'
export const server_join_date = 'Server join date'
export const account_creation_date = 'Account creation date'
export const roles = 'Roles'
export function avatar(user) { return `${user}'${user.endsWith('s') ? '' : 's'} avatar` }
export const bulk_delete_two_weeks = 'Messages older than 2 weeks can not be removed.'
export const bulk_delete_missing_permissions = 'I do not have permission to delete messages in this channel.'
export const bulk_delete_max_100 = 'You can only remove less than 100 messages at once.'
export const no_images_found = `Couldn't find any images matching that query.`
export const jump_to_moment = 'Jump to moment'
export function the_current_prefix_for_this_server_is(prefix) { return `The current prefix for this server is ${prefix}` }
export function change_prefix_to(prefix) { return `Change the prefix to **${prefix}**?` }
export function prefix_was_changed_to(prefix) { return `Prefix was changed to **${prefix}**` }
export const valid_modules = 'Valid modules'
export const available_languages = 'Available languages'
export function boop_title(user) { return `${user[0]} booped ${user[1]}!` }
export function boop_self(user) { return `${user} booped themself` }
export function boop_chrysalis(user) { return `${user} tried to boop Chrysalis` }
export function hug_title(user) { return `${user[0]} hugged ${user[1]}` }
export function hug_self(user) { return `${user} hugged themself` }
export function hug_chrysalis(user) { return `${user} tried to hug Chryalis` }
export function kiss_title(user) { return `${user[0]} kissed ${user[1]}` }
export function kiss_self(user) { return `${user} kissed themself` }
export function kiss_chrysalis(user) { return `${user} tried to kiss Chrysalis` }
export const server_info = 'Server info:'
export const server_id = 'Server ID'
export const server_owner = 'Server owner'
export const channels = 'Channels'
export const server_boosts = 'Server boosts'
export const image_source = 'Image source'
export const please_specify_a_new_value = 'Please, specify a new value.'
export const value_must_be_true_or_false = 'The value must be **true** or **false**.' // Do NOT translate **true** and **false**
export const module_updated = 'Module updated!'
export const attachments = 'Attachments'
export const message_id = 'Message ID'
export function module_enabled(m) { return `Module ${m} enabled!` }
export function module_disabled(m) { return `Module ${m} disabled!` }
export function module_already_enabled(m) { return `Module  ${m} was already enabled.` }
export function module_already_disabled(m) { return `Module  ${m} was already disabled.` }
export const current_color = 'Current color'
export function change_color_to(hex) { return `Change color to ${hex}?` }
export const invalid_color = 'Invalid color.'
export function color_was_changed_to(hex) { return `Color was changed to ${hex}` }
export const role_menu = 'Role menu'
export const role_menu_not_found = `Couldn't find a role menu in the provided URL`
export const select_the_roles_that_you_want = 'Select the roles that you want'
export const no_valid_roles_found = 'No valid roles found. Type role IDs separated by line breaks.'
export const you_can_only_add_up_to_25_roles_to_the_menu = 'You can only add up to 25 roles to the menu.'
export const invalid_roles_skipped = 'Invalid roles were skipped.'
export const manage_roles_permission_required = 'Chrysalis needs the `MANAGE_ROLES` permission for role menus to work.'
export const chrysalis_role_too_low = `Chrysalis' role is lower than the role you're requesting. Please, ask an admin to fix it.`
export const roles_managed_by_integrations_cannot_be_manually_assigned = 'Roles managed by integrations cannot be manually assigned.'
export const download_emoji = 'Download emoji'
export const please_type_a_valid_channel = 'Please, type a valid channel.'
export const invalid_channel = 'At least one of the channels you typed is invalid.'
export const invite_the_bot = 'Invite the bot'
export const website = 'Website'
export const support_server = 'Support Server'
export const source_code = 'Source code'
export const support_the_project = 'Support the project'
export const unknown_role = `Couldn't find that role.`
export const role_id = 'Role ID'
export const welcome = 'Welcome!'
export function you_are_the_member_n(n) { return `You are the member Nº${n}` }
export const filter_not_found = 'Filter not found. Please, make sure that the filter is public.'
export const help_time_out = 'Time to move between pages is over.'
export const old_message = 'Old message'
export const new_message = 'New message'
export const old_attachments = 'Old attachments'
export const new_attachments = 'New attachments'
export const message_edited = 'Message edited'
export const season = 'Season'
export const seasons = 'Seasons'
export const movies = 'Movies'
export const torrent_footer = 'Chrysalis recommends using qBittorrent, since it is ad-free and open source.'
export const error_fetching_episodes = 'There was an error fetching the episodes. Please, try again later.'
export const attach_files_permission_missing = `Chrysalis doesn't have permission to send images to this channel.`
export const embed_links_permission_missing = `Chrysalis doesn't have permission to send embeds to this channel. Please give Chrysalis the "Embed links" permission.`
export const please_type_a_valid_positive_integer = 'Please type a valid positive integer.'
export const module_property_not_found = 'Module property not found.'
export function levelup(level) { return `**Level up!**\nYou're level **${level}** now!` }
export const leaderboard_title = 'Leaderboard'
export const level = 'Level'
export const rank = 'Rank'
export const total_xp = 'Total XP'
export const profile = 'Profile'
export const profile_updated = 'Profile updated!'
export const unsupported_image_type = 'Unsupported image type.'
export const check_documentation = 'Click on the module name to open the documentation for this module.'
export const import_levels_from = 'Which bot do you want to import levels from?'
export function no_levels_found(bot) { return `No levels found from ${bot} on this server.` }
export function mee6_fix(guildID) { return `Please make sure your MEE6 leaderboard is public by checking [this option](https://mee6.xyz/dashboard/${guildID}/leaderboard).` }
export const xp_migration_adapt = 'The way Chrysalis calculates levels may vary from other bots. What do you want to do?'
export const import_levels_and_adapt_xp = 'Import levels and adapt XP'
export const import_xp_and_adapt_levels = 'Import XP and adapt levels'
export const import_leaderboard = 'The leaderboard will now look like this:'
export const migration_complete = 'Migration complete!'
export const xp_successfully_imported = 'XP has been successfully imported.'
export const edit = 'Edit'
export const module_time_out = 'Time to edit this module is over.'
export const use_modal_instead = 'Banned words cannot be edited this way. Please, use the Edit button.'
export function messages_deleted(n) { return `${n} message${n > 1 ? 's' : ''} deleted! ${PinkiePieYes}` }
export const optional = 'optional'
export const click_to_open_modal = `Since you're not using a slash command, you'll need to click this button to open the modal.`
export function captcha_attempts_left(n) { return `Incorrect. You have **${n}** attempt${n == 1 ? '' : 's'} left.` }
export const verification = 'Verification'

// Modal input field labels - Max 45 characters
export const enabled = 'Enabled'
export const logDeletedMessages = 'Log deleted messages'
export const logEditedMessages = 'Log edited messages'
export const words = 'Words/phrases (separated by line breaks)'
export const title = 'Title'
export const description = 'Description'
export const reactToLinks = 'React to links'
export const reactToFiles = 'React to files'
export const approvalEmoji = 'Approval emoji'
export const disapprovalEmoji = 'Disapproval emoji'
export const restricted = 'Command restricted to certain channels'
export const allowedChannels = 'Channels the command is restricted to'
export const filter = 'Filter'
export const background = 'Background'
export const banMessage = 'Ban message'
export const xpBlacklistChannels = 'Channels where XP cannot be earned'
export const xpPerMessage = 'XP earned per message'
export const messageCooldown = 'XP cooldown on messages (in seconds)'
export const xpInVoiceChat = 'XP earned in VC'
export const voiceChatCooldown = 'How often to earn XP in VC (in seconds)'
export const announceLevelUp = 'Announce level ups'
export const announceLevelUpChannel = 'Level up announcements channel'
export const sendMessageOnDelete = 'Send DM after deleting message'
export const ignoreAdmins = 'Ingore admin messages'
export const role = 'Role'
export const requiresCaptcha = 'Requires captcha'
export const captchaChannel = 'Captcha channel'
export const captchaMaxAttempts = 'Captcha max attempts'
export const captchaTimeout = 'Captcha timeout'
export const roleBeforeCaptcha = 'Role before captcha'
export const captchaDescription = 'Captcha description'
export const captchaKickMessage = 'Captcha kick message'
export const color = 'Color'
export const background_image = 'Background image'
