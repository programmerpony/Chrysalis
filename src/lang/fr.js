/*

 Copyright (C) 2023-2024 programmerpony
 Copyright (C) 2023 Soulphase

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { ApplicationCommandOptionType } from 'discord.js';
import defaultModules from '../defaultModules.js';
import { NitroBoost, PinkiePieYes } from '../emojis.js';

export const meta = {
  name: 'Français',
  new_lang_message: `Je m’exprimerai désormais en français.`
}

/*
DO NOT TRANSLATE COMMAND NAMES, only descriptions, option names and option descriptions
Command description - Max 100 characters
Command option name - Max 32 chracters
Command option description - Max 100 characters
*/
export const commands = {
  user: [
    {
      name: 'avatar',
      description: 'Renvoie l’avatar d’un utilisateur.',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur dont l’avatar sera renvoyé.'
        }
      ]
    },
    {
      name: 'userinfo',
      description: 'Affiche les informations relatives à un utilisateur.',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur dont les informations seront affichées.'
        }
      ]
    },
    {
      name: 'roleinfo',
      description: 'Affiche les informations relatives à un rôle.',
      options: [
        {
          name: 'rôle',
          type: ApplicationCommandOptionType.Role,
          description: 'Rôle dont les informations seront affichées.',
          required: true
        }
      ]
    },
    {
      name: 'serverinfo',
      description: 'Affiche les informations relatives au serveur.'
    },
    {
      name: 'rank',
      description: 'Affiche votre niveau actuel ainsi que votre expérience.',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur dont l’expérience et le niveau seront affichés'
        }
      ]
    },
    {
      name: 'leaderboard',
      description: 'Affiche le classement des utilisateurs par niveau et expérience.'
    },
    {
      name: 'profile',
      description: 'Modifie votre profil (sur tous les serveurs).'
    },
    {
      name: 'emoji',
      description: 'Renvoie l’émoji sous la forme d’une image.',
      options: [
        {
          name: 'émoji',
          type: ApplicationCommandOptionType.String,
          description: 'Émoji à convertir.',
          required: true
        }
      ]
    },
    {
      name: 'love',
      description: 'Détermine le degré de compatibilité entre deux utilisateurs.',
      options: [
        {
          name: 'premier_membre',
          type: ApplicationCommandOptionType.User,
          description: 'Premier membre du couple.',
          required: true
        },
        {
          name: 'second_membre',
          type: ApplicationCommandOptionType.User,
          description: 'Second membre du couple.',
          required: true
        }
      ]
    },
    {
      name: 'boop',
      description: 'Boop un utilisateur',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur à booper.',
          required: true
        }
      ]
    },
    {
      name: 'hug',
      description: 'Enlace un utilisateur.',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur à enlacer.',
          required: true
        }
      ]
    },
    {
      name: 'kiss',
      description: 'Embrasse un utilisateur.',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur à embrasser.',
          required: true
        }
      ]
    },
    {
      name: 'booru',
      description: 'Renvoie une image provenant de Manebooru.',
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags pour la recherche (séparés par des virgules).'
        }
      ]
    },
    {
      name: 'clop',
      description: 'Renvoie une image explicite (CLOP) provenant de Manebooru.',
      nsfw: true,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags pour la recherche (séparés par des virgules).'
        }
      ]
    },
    {
      name: 'e621',
      description: 'Renvoie une image explicite provenant de e621.',
      nsfw: true,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags pour la recherche (séparés par des virgules).'
        }
      ]
    },
    {
      name: 'torrent',
      description: 'Torrents de poneys !'
    },
    {
      name: 'say',
      description: 'Chrysalis dit ce que vous saisissez.',
      options: [
        {
          name: 'texte',
          type: ApplicationCommandOptionType.String,
          description: 'Texte que Chrysalis devra dire.',
          required: true
        }
      ]
    }
  ],
  admin: [
    {
      name: 'config',
      description: `Modifie les modules de Chrysalis.`,
      options: [
        {
          name: 'module',
          type: ApplicationCommandOptionType.String,
          description: 'Choisissez le module à modifier.',
          choices: defaultModules.map(m => { return { name: m.name, value: m.name } }),
          required: true
        }
      ]
    },
    {
      name: 'clean',
      description: 'Suppression des messages en bloc.',
      options: [
        {
          name: 'nombre_messages',
          type: ApplicationCommandOptionType.Integer,
          description: 'Nombre de messages à supprimer.',
          minValue: 1,
          maxValue: 100,
          required: true
        }
      ]
    },
    {
      name: 'lang',
      description: `Changer la langue de Chrysalis.`,
      options: [
        {
          name: 'langue',
          type: ApplicationCommandOptionType.String,
          description: 'Langue dans laquelle Chrysalis devra s’exprimer.',
          choices: [
            {
              name: 'Anglais',
              value: 'en'
            },
            {
              name: 'Espagnol',
              value: 'es'
            },
            {
              name: 'Italien',
              value: 'it'
            },
            {
              name: 'Français',
              value: 'fr'
            }
          ],
          required: true
        }
      ]
    },
    {
      name: 'color',
      description: 'Changer la couleur par défaut des « embeds ».',
      options: [
        {
          name: 'couleur',
          type: ApplicationCommandOptionType.String,
          description: 'Couleur sous la forme hexadécimale.',
          maxLength: 7
        }
      ]
    },
    {
      name: 'welcome',
      description: 'Envoie une carte de bienvenue à un utilisateur donné.',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur à accueillir.'
        }
      ]
    },
    {
      name: 'boost',
      description: 'Envoie une carte de remerciement pour le boost à un utilisateur donné.',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur à remercier pour le soutien.'
        }
      ]
    },
    {
      name: 'setxp',
      description: 'Fixe la quantité d’expérience d’un utilisateur donné.',
      options: [
        {
          name: 'utilisateur',
          type: ApplicationCommandOptionType.User,
          description: 'Utilisateur dont l’expérience sera fixé.',
          required: true
        },
        {
          name: 'xp',
          type: ApplicationCommandOptionType.Integer,
          description: 'Nouvelle quantité d’expérience.',
          minValue: 0,
          maxValue: Number.MAX_SAFE_INTEGER,
          required: true
        }
      ]
    },
    {
      name: 'importxp',
      description: 'Importe l’expérience acquise auprès d’autres robots !'
    },
    {
      name: 'rolemenu',
      description: 'Établit un menu permettant aux utilisateurs de choisir leurs rôles !',
      options: [
        {
          name: 'editer',
          type: ApplicationCommandOptionType.String,
          description: 'Modifie un menu de rôles existant. Passer en argument le lien vers le message en question.'
        }
      ]
    }
  ]
}
export const defaultValues = {
  bannedwords: {
    message: 'Ce message a été supprimé parce qu’il contenait quelque chose que le serveur interdit. Faites attention à ce que vous dites, merci.'
  },
  boost: {
    message: '**{user} vient de booster le serveur !**',
    title: 'Nous t’en remercions tous !',
    description: `${NitroBoost} Profites de ce rôle exclusif ! ${NitroBoost}`
  },
  welcome: {
    message: 'Bienvenue sur **{guild}** ! {user}'
  },
  goodbye: {
    message: 'Au revoir, **{user}**. Nous espérons vous revoir bientôt.',
    banMessage: '**{user}** a été banni du serveur.'
  },
  autorole: {
    captchaDescription: 'Résolvez ce captcha pour accéder à **{guild}**. Vous disposez de {seconds} secondes avant l’expulsion automatique.',
    captchaKickMessage: 'Vous avez été expulsé de **{guild}** pour défaut de résolution du captcha.'
  }
}
export const author = 'Auteur'
export const message = 'Message'
export const channel = 'Canal'
export const user_commands = 'Commandes:'
export const admin_commands = 'Commandes Administratives:'
export const wrong_channel = 'La commande envoyée est inopérante ici.'
export const message_deleted = 'Message supprimé'
export const you_must_send_an_emoji = 'Veuillez saisir un émoji.'
export const couldn_t_find_that_emoji = `Impossible de trouver cet émoji.`
export const type_one_or_two_users = 'Saisissez un ou deux utilisateurs.'
export const self_love = `Je sais que l’amour propre est primordial, mais n’en soyez pas narcissique pour autant.`
export const lovemeter_messages = [
  'Sans la moindre compatibilité. Triste.',
  'Improbable, mais encore possible.',
  'Très faible... amis sans doute.',
  'Probablement juste amis.',
  'Peu probable, mais il pourrait y avoir quelque chose.',
  `Il se peut qu’il y ait quelque chose, mais je préfère éviter de donner de faux espoirs.`,
  'Very likely to be something. You can try.',
  'Très compatible. Ils formeraient un joli couple.',
  'Ces deux-là s’embrassent à coup sûr !',
  'Ils formeraient un merveilleux couple !',
  'Destinés à rester unis pour la vie !'
]
export const nsfw_only = 'Cette commande peut uniquement être utilisée dans les canaux explicites (NSFW).'
export const requested_by = 'Sollicité par'
export const how_to_delete = 'Cliquer sur ce bouton si l’image proposée est inappropriée.'
export const please_report = `Signaler cette image pour prévenir sa réapparition.`
export const role_info = 'Informations sur le rôle:'
export const name = 'Nom'
export const member_count = 'Nombre de membres'
export const date_created = 'Date de Création'
export const couldn_t_find_that_user = `Impossible de trouver cet utilisateur.`
export const user_info = 'Informations sur l’utilisateur:'
export const user_id = 'Identifiant utilisateur'
export const server_join_date = 'Date d’entrée sur le serveur'
export const account_creation_date = 'Date de création du compte'
export const roles = 'Rôles'
export function avatar(user) { return `Avatar de ${user}` }
export const bulk_delete_two_weeks = 'Les messages datant de plus de 2 semaines ne peuvent pas être supprimés.'
export const bulk_delete_missing_permissions = 'Je ne suis pas autorisée à supprimer les messages de ce canal.'
export const bulk_delete_max_100 = 'Seuls 100 messages peuvent être supprimés par itération.'
export const no_images_found = `Aucune image ne correspond à cette demande.`
export const jump_to_moment = 'Passer à l’instant présent'
export function the_current_prefix_for_this_server_is(prefix) { return `Le préfixe utilisé par ce serveur est « ${prefix} ».` }
export function change_prefix_to(prefix) { return `Changer le préfixe pour « **${prefix}** » ?` }
export function prefix_was_changed_to(prefix) { return `Le préfixe a été modifié en « **${prefix}** ».` }
export const valid_modules = 'Valider les modules'
export const available_languages = 'Langues disponibles'
export function boop_title(user) { return `${user[0]} boope ${user[1]} !` }
export function boop_self(user) { return `${user} s’est lui-mêmes boupé(e) !` }
export function boop_chrysalis(user) { return `${user} a essayé de booper Chrysalis` }
export function hug_title(user) { return `${user[0]} enlace ${user[1]}` }
export function hug_self(user) { return `${user} s’enlace avec eux-mêmes !` }
export function hug_chrysalis(user) { return `${user} a essayé d’enlacer Chryalis` }
export function kiss_title(user) { return `${user[0]} embrasse ${user[1]} !` }
export function kiss_self(user) { return `${user} s’est lui-mêmes embrassé(e) !` }
export function kiss_chrysalis(user) { return `${user} a essayé d’embrasser Chrysalis` }
export const server_info = 'Informations serveur:'
export const server_id = 'Indentifiant serveur'
export const server_owner = 'Propriétaire du serveur'
export const channels = 'Canaux'
export const server_boosts = 'Boosts serveur'
export const image_source = 'Source de l’image'
export const please_specify_a_new_value = 'Veuillez indiquer une nouvelle valeur.'
export const value_must_be_true_or_false = 'La valeur doit être **true** ou **false**.' // Do NOT translate **true** and **false**
export const module_updated = 'Module mis-à-jour !'
export const attachments = 'Pièces-jointes'
export const message_id = 'Identifiant message'
export function module_enabled(m) { return `Module ${m} activé !` }
export function module_disabled(m) { return `Module ${m} désactivé !` }
export function module_already_enabled(m) { return `Module ${m} est déjà activé.` }
export function module_already_disabled(m) { return `Module ${m} est déjà désactivé.` }
export const current_color = 'Couleur actuel'
export function change_color_to(hex) { return `Changer pour la couleur ${hex}?` }
export const invalid_color = 'Couleur invalide.'
export function color_was_changed_to(hex) { return `Couleur modifiée en ${hex}` }
export const role_menu = 'Menu de rôles'
export const role_menu_not_found = `Impossible de trouver le menu de rôles avec l’adresse fournie.`
export const select_the_roles_that_you_want = 'Sélectionnez les rôles souhaités'
export const no_valid_roles_found = 'Aucun rôle valide trouvé. Saisir les identifiants des rôles en les séparant par des retours-à-la-ligne.'
export const you_can_only_add_up_to_25_roles_to_the_menu = 'Vous ne pouvez ajouter plus de 25 rôles au menu.'
export const invalid_roles_skipped = 'Les rôles non-valides ont été ignorés.'
export const manage_roles_permission_required = 'Chrysalis a besoin de l’autorisation `MANAGE_ROLES` pour le fonctionnement du menu de rôles.'
export const chrysalis_role_too_low = `Le rôle de Chrysalis est inférieur au rôle demandé. Veuillez demander à un administrateur d’y remédier.`
export const roles_managed_by_integrations_cannot_be_manually_assigned = 'Les rôles gérés par des intégrations ne peuvent pas être attribués manuellement.'
export const download_emoji = 'Télécharger l’émoji'
export const please_type_a_valid_channel = 'Veuillez saisir un canal valide.'
export const invalid_channel = 'Un des canaux saisis est invalide.'
export const invite_the_bot = 'Inviter le robot'
export const website = 'Site'
export const support_server = 'Serveur d’Assistance'
export const source_code = 'Code Source'
export const support_the_project = 'Soutenir le projet'
export const unknown_role = `Impossible de trouver ce rôle.`
export const role_id = 'Identifiant rôle'
export const welcome = 'Bienvenue !'
export function you_are_the_member_n(n) { return `Vous êtes le membre №${n} !` }
export const filter_not_found = 'Filtre introuvable. Veuillez vous assurer que le filtre est public.'
export const help_time_out = 'Le temps s’est écoulé ! Il n’est plus possible de passer d’une page à l’autre.'
export const old_message = 'Ancien message'
export const new_message = 'Nouveau message'
export const old_attachments = 'Anciennes pièces-jointes'
export const new_attachments = 'Nouvelles pièces-jointes'
export const message_edited = 'Message edité'
export const season = 'Saison'
export const seasons = 'Saisons'
export const movies = 'Films'
export const torrent_footer = 'Chrysalis recommande d’utiliser qBittorrent, puisqu’il est libre et sans publicité.'
export const error_fetching_episodes = 'Une erreur s’est produite lors de la récupération des épisodes. Veuillez réessayer plus tard.'
export const attach_files_permission_missing = `Chrysalis ne dispose pas l’autorisation d’envoyer des images sur ce canal.`
export const embed_links_permission_missing = `Chrysalis ne dispose pas de l’autorisation d’envoyer des liens vers ce canal. Veuillez accorder à Chrysalis l'autorisation « Embed links ».`
export const please_type_a_valid_positive_integer = 'Veuillez saisir un nombre entier positif valide.'
export const module_property_not_found = 'Propriété du module introuvable.'
export function levelup(level) { return `**Niveau élevé !**\nVous êtes atteint le niveau **${level}** !` }
export const leaderboard_title = 'Classement'
export const level = 'Niveau'
export const rank = 'Rang'
export const total_xp = 'Expérience Totale'
export const profile = 'Profil'
export const profile_updated = 'Profil mis-à-jour !'
export const unsupported_image_type = 'Type d’image non pris en charge.'
export const check_documentation = 'Cliquez sur l’intitulé pour ouvrir la documentation relative au module.'
export const import_levels_from = 'Quels sont les robots à partir desquels les niveaux doivent être importés ?'
export function no_levels_found(bot) { return `Aucun système de niveau détecté pour ${bot}.` }
export function mee6_fix(guildID) { return `Veuillez vous assurer que votre classement MEE6 est public en cochant [cette option](https://mee6.xyz/dashboard/${guildID}/leaderboard).` }
export const xp_migration_adapt = 'La façon dont Chrysalis calcule les niveaux et l’expérience peut différer de celle des autres robots. Que voulez-vous faire ?'
export const import_levels_and_adapt_xp = 'Importer les niveaux et adapter l’expérience'
export const import_xp_and_adapt_levels = 'Importer l’expérience et adapter les niveaux'
export const import_leaderboard = 'Le classement ressemblera alors à ceci:'
export const migration_complete = 'Migration complète !'
export const xp_successfully_imported = 'L’expérience a été importé avec succès.'
export const edit = 'Éditer'
export const module_time_out = 'Le temps s’est écoulé ! Il n’est plus possible d’éditer ce module.'
export const use_modal_instead = 'La liste de mots interdits ne peut être modifié de cette manière. Veuillez utiliser le bouton Editer.'
export function messages_deleted(n) { return `${n} message${n > 1 ? 's' : ''} supprimée${n > 1 ? 's' : ''} ! ${PinkiePieYes}` }
export const optional = 'facultatif'
export const click_to_open_modal = `Puisque vous n’utilisez pas de commande slash, il vous faudra cliquer sur le bouton pour ouvrir la fenêtre modale.`
export function captcha_attempts_left(n) { return `Mauvaise réponse. Il vous reste **${n}** tentative${n == 1 ? '' : 's'}.` }
export const verification = 'Vérification'

// Modal input field labels - Max 45 characters
export const enabled = 'État du module (activé ou désactivé)'
export const logDeletedMessages = 'Enregistrer les messages supprimés'
export const logEditedMessages = 'Enregistrer les messages édités'
export const words = 'Mots/Phrases (séparés de retours-à-la-ligne)'
export const title = 'Titre'
export const description = 'Description'
export const reactToLinks = 'Réagir aux liens'
export const reactToFiles = 'Réagir aux fichiers'
export const approvalEmoji = 'Émoji d’approbation'
export const disapprovalEmoji = 'Émoji de désaccord'
export const restricted = 'Commande limitée à certains canaux'
export const allowedChannels = 'La commande est restreinte aux canaux:'
export const filter = 'Filtre'
export const background = 'Arrière-plan'
export const banMessage = 'Message de bannissement'
export const xpBlacklistChannels = `Canaux où l’expérience ne peut être obtenue`
export const xpPerMessage = 'Expérience gagnée par message'
export const messageCooldown = `Cooldown expérience par message (en secs)`
export const xpInVoiceChat = 'Expérience gagnée dans un canal-vocal'
export const voiceChatCooldown = 'Cooldown expérience en vocal (en secs)'
export const announceLevelUp = 'Annoncer les montées de niveau'
export const announceLevelUpChannel = 'Canal d’annonces de niveaux'
export const sendMessageOnDelete = 'Envoyer un MP à la suppression d’un message'
export const ignoreAdmins = 'Ignorer les messages administratifs'
export const role = 'Rôle'
export const requiresCaptcha = 'Nécessite le captcha'
export const captchaChannel = 'Cannal du captcha'
export const captchaMaxAttempts = 'Tentatives maximum du captcha'
export const captchaTimeout = 'Délai de dépassement du captcha'
export const roleBeforeCaptcha = 'Rôle avant captcha'
export const captchaDescription = 'Description du captcha'
export const captchaKickMessage = 'Message d’expulsion du captcha'
export const color = 'Couleur'
export const background_image = 'Image de fond'