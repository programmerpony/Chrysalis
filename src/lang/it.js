/*

 Copyright (C) 2022-2024 programmerpony
 Copyright (C) 2022 Princess Celestia

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { ApplicationCommandOptionType } from 'discord.js';
import defaultModules from '../defaultModules.js';
import { NitroBoost, PinkiePieYes } from '../emojis.js';

export const meta = {
  name: 'Italiano',
  new_lang_message: 'Parlerò in italiano.'
}

/*
DO NOT TRANSLATE COMMAND NAMES, only descriptions, option names and option descriptions
Command description - Max 100 characters
Command option name - Max 32 chracters
Command option description - Max 100 characters
*/
export const commands = {
  user: [
    {
      name: 'avatar',
      description: `Invia il avatar dell'utente menzionato.`,
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `L'utente di cui vuoi inviare l'avatar`
        }
      ]
    },
    {
      name: 'userinfo',
      description: `Mostra informazioni account dell'utente menzionato.`,
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `L'utente di cui vuoi mostrare l'informazione.`
        }
      ]
    },
    {
      name: 'roleinfo',
      description: 'Mostra informazioni sul ruolo.',
      options: [
        {
          name: 'ruolo',
          type: ApplicationCommandOptionType.Role,
          description: `Il ruolo di cui vuoi mostrare l'informazione.`,
          required: true
        }
      ]
    },
    {
      name: 'serverinfo',
      description: 'Mostra informazioni sul server.'
    },
    {
      name: 'rank',
      description: 'Mostra il tuo XP e livello nel tabellone.',
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `Il grado dell'utente da visualizzare.`
        }
      ]
    },
    {
      name: 'leaderboard',
      description: 'Mostra gli utenti con più XP.'
    },
    {
      name: 'profile',
      description: 'Modifica il tuo profilo (in ogni server).'
    },
    {
      name: 'emoji',
      description: `Invia l'emoji come un'immagine.`,
      options: [
        {
          name: 'emoji',
          type: ApplicationCommandOptionType.String,
          description: `L'emoji di cui vuoi cambiare in un'immagine.`,
          required: true
        }
      ]
    },
    {
      name: 'love',
      description: 'Verifica la compatibilità tra due utenti.',
      options: [
        {
          name: 'amante1',
          type: ApplicationCommandOptionType.User,
          description: 'Primo membro della coppia.',
          required: true
        },
        {
          name: 'amante2',
          type: ApplicationCommandOptionType.User,
          description: 'Secondo membro della coppia.',
          required: true
        }
      ]
    },
    {
      name: 'boop',
      description: `Boop un'utente.`,
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `L'utente di cui vuoi fare un boop.`,
          required: true
        }
      ]
    },
    {
      name: 'hug',
      description: `Abbraccia un'utente.`,
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `L'utente di cui vuoi abbracciare.`,
          required: true
        }
      ]
    },
    {
      name: 'kiss',
      description: `Bacia un'utente.`,
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `L'utente che vuoi baciare`,
          required: true
        }
      ]
    },
    {
      name: 'booru',
      description: `Invia un'immagine SFW da Manebooru.`,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Parola chiave da ricercare (separare con virgole).'
        }
      ]
    },
    {
      name: 'clop',
      description: `Invia un'immagine NSFW da Manebooru.`,
      nsfw: true,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Parola chiave da ricercare (separare con virgole).'
        }
      ]
    },
    {
      name: 'e621',
      description: `Invia un'immagine da e621.`,
      nsfw: true,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Parola chiave da ricercare (separare con spazi).'
        }
      ]
    },
    {
      name: 'torrent',
      description: 'Torrent pony!'
    },
    {
      name: 'say',
      description: 'Chrysalis dice quello che scrivi.',
      options: [
        {
          name: 'testo',
          type: ApplicationCommandOptionType.String,
          description: 'Il testo vuoi Chyrsalis dire.',
          required: true
        }
      ]
    }
  ],
  admin: [
    {
      name: 'config',
      description: `Modifica un modulo`,
      options: [
        {
          name: 'modulo',
          type: ApplicationCommandOptionType.String,
          description: 'Scegli il modulo che vuoi modificare.',
          choices: defaultModules.map(m => { return { name: m.name, value: m.name } }),
          required: true
        }
      ]
    },
    {
      name: 'clean',
      description: 'Elimina più messaggi contemporaneamente.',
      options: [
        {
          name: 'messaggi',
          type: ApplicationCommandOptionType.Integer,
          description: 'La quantità di messaggi che desideri eliminare.',
          minValue: 1,
          maxValue: 100,
          required: true
        }
      ]
    },
    {
      name: 'lang',
      description: 'Cambia la lingua di Chrysalis',
      options: [
        {
          name: 'lingua',
          type: ApplicationCommandOptionType.String,
          description: 'La lingua in cui vuoi che Chrysalis parli.',
          choices: [
            {
              name: 'Inglese',
              value: 'en'
            },
            {
              name: 'Spagnolo',
              value: 'es'
            },
            {
              name: 'Italiano',
              value: 'it'
            },
            {
              name: 'Francese',
              value: 'fr'
            }
          ],
          required: true
        }
      ]
    },
    {
      name: 'color',
      description: 'Cambia il colore predefinito per gli incorporamenti.',
      options: [
        {
          name: 'color',
          type: ApplicationCommandOptionType.String,
          description: 'Deve essere un colore esadecimale.',
          maxLength: 7
        }
      ]
    },
    {
      name: 'welcome',
      description: `Invia un biglietto di benvenuto per l'utente specificato.`,
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `L'utente a cui vuoi dare il benvenuto.`
        }
      ]
    },
    {
      name: 'boost',
      description: `Invia una carta boost per l'utente specificato.`,
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `L'utente per cui vuoi mostrare la carta boost.`
        }
      ]
    },
    {
      name: 'setxp',
      description: `Imposta la quantità di XP che l'utente specificato ha.`,
      options: [
        {
          name: 'utente',
          type: ApplicationCommandOptionType.User,
          description: `L'utente di cui si desidera impostare l'XP.`,
          required: true
        },
        {
          name: 'xp',
          type: ApplicationCommandOptionType.Integer,
          description: 'La nuova quantità di XP.',
          minValue: 0,
          maxValue: Number.MAX_SAFE_INTEGER,
          required: true
        }
      ]
    },
    {
      name: 'importxp',
      description: 'Importa XP da robot di altri livelli!'
    },
    {
      name: 'rolemenu',
      description: 'Imposta un menu da cui gli utenti possono scegliere i ruoli!',
      options: [
        {
          name: 'modificare',
          type: ApplicationCommandOptionType.String,
          description: `Se desideri modificare un menu di ruolo già esistente, incolla qui l'URL del messaggio.`
        }
      ]
    }
  ]
}
export const defaultValues = {
  bannedwords: {
    message: 'Il tuo messaggio era eliminato perché contiene parole non consentito in questo server. Stai attento a quello che dici!'
  },
  boost: {
    message: '**{user} ha appena potenziato il server!**',
    title: 'Grazie mille per aver poteniziato il server!',
    description: `${NitroBoost} Goditi il tuo nuovo ruolo! ${NitroBoost}`
  },
  welcome: {
    message: 'Benvenuto a **{guild}**! {user}'
  },
  goodbye: {
    message: 'Arrivederci, **{user}**. Spero ci vediamo presto.',
    banMessage: '**{user}** è stato bannato dal server.'
  },
  autorole: {
    captchaDescription: `Risolvi questo captcha per ottenere l'accesso a **{gilda}**. Hai {seconds} secondi prima di essere espulso automaticamente.`,
    captchaKickMessage: 'Sei stato espulso da **{guild}** per non aver risolto il captcha.'
  }
}
export const author = 'Autore'
export const message = 'Messaggio'
export const channel = 'Canale'
export const user_commands = 'Comandi:'
export const admin_commands = 'Comandi Admin:'
export const wrong_channel = 'Il comando che hai inviato non può essere usato in questo canale'
export const message_deleted = 'Messaggio eliminato'
export const you_must_send_an_emoji = `Si deve inviare un'emoji`
export const couldn_t_find_that_emoji = `Non può trovare l'emoji inviato.`
export const type_one_or_two_users = 'Digitare un o due utenti.'
export const self_love = 'So che è importante amare se stessi, ma non essere un narcisista!'
export const lovemeter_messages = [
  'Triste.',
  'Incompatibile.',
  'Molto Basso... Solo siete amici.',
  'Probabile amici.',
  'Non è probabile ma non impossibile.',
  'Potrebbe!',
  'È probabile. Voi potete provare.',
  'Molto compatibile! Potreste essere molto carini',
  'Baciate definitivamente.',
  'Potreste essere una relazione meravigliosa',
  'Potreste stare insieme per sempre'
]
export const nsfw_only = 'Questo comando solo può essere usato in un canale NSFW.'
export const requested_by = 'Richiesto di'
export const how_to_delete = 'Clicca qui se questa immagine è inappropriate.'
export const please_report = 'Segnala questa immagine in modo che non appaia più.'
export const role_info = 'Info del ruolo:'
export const name = 'Nome'
export const member_count = 'Conte membro'
export const date_created = 'Data di creazione'
export const couldn_t_find_that_user = `Impossibile trovare l'utente .`
export const user_info = `Informazioni dell'utente:`
export const user_id = `ID dell'utente`
export const server_join_date = 'Data di iscrizione al server'
export const account_creation_date = `Data di creazione dell'account`
export const roles = 'Ruoli'
export function avatar(user) { return `Avatar da ${user}` }
export const bulk_delete_two_weeks = 'Messaggi più vecchie di due semane non può essere eliminato.'
export const bulk_delete_missing_permissions = 'Non ho i permissi necessari per ne elimnare in questo canale.'
export const bulk_delete_max_100 = 'Solo puoi elimnare meno che 100 messaggi.'
export const no_images_found = 'Impossibile trovare immagini corrispondenti a quella ricerca.'
export const jump_to_moment = 'Passa al momento'
export function the_current_prefix_for_this_server_is(prefix) { return `Il prefisso attuale per questo server è ${prefix}` }
export function change_prefix_to(prefix) { return `Cambia el prefisso con **${prefix}**?` }
export function prefix_was_changed_to(prefix) { return `Prefisso stato cambiato con **${prefix}**` }
export const valid_modules = 'Moduli validi'
export const available_languages = 'Lingue disponibili'
export function boop_title(user) { return `${user[0]} ha fatto un boop su ${user[1]}!` }
export function boop_self(user) { return `${user} ha fatto un boop su se stesso` }
export function boop_chrysalis(user) { return `${user} provava fare un boop a Chrysalis` }
export function hug_title(user) { return `${user[0]} ha abbracciato ${user[1]}` }
export function hug_self(user) { return `${user} ha abbracciato se stesso` }
export function hug_chrysalis(user) { return `${user} ha provato abbracciare Chrysalis.` }
export function kiss_title(user) { return `${user[0]} ha baciato ${user[1]}` }
export function kiss_self(user) { return `${user} si bacia` }
export function kiss_chrysalis(user) { return `${user} provava baciare Chrysalis` }
export const server_info = 'Informazioni del server:'
export const server_id = 'ID del server'
export const server_owner = 'Proprietario del server'
export const channels = 'Canali'
export const server_boosts = 'Potenziamenti'
export const image_source = `Fonte dell'immagine`
export const please_specify_a_new_value = 'Specifica un valore nuovo.'
export const value_must_be_true_or_false = 'Il valore deve essere **true** o **false**.' // Do NOT translate **true** and **false**
export const module_updated = 'Il modulo è aggiornato!'
export const attachments = 'Allegati'
export const message_id = 'ID del messaggio'
export function module_enabled(m) { return `Modulo ${m} sta abilitato.` }
export function module_disabled(m) { return `Modulo ${m} sta disabilitato.` }
export function module_already_enabled(m) { return `Il modulo ${m} era già abilitato.` }
export function module_already_disabled(m) { return `Il modulo ${m} era già disabilitato.` }
export const current_color = 'Colore attuale'
export function change_color_to(hex) { return `Cambia il colore in ${hex}?` }
export const invalid_color = 'Colore non valido.'
export function color_was_changed_to(hex) { return `Il colore sta cambiato in ${hex}` }
export const role_menu = 'Menu ruolo '
export const role_menu_not_found = `Impossibile trovare un menu di ruolo nell'URL specificato`
export const select_the_roles_that_you_want = 'Seleziona i ruoli che desideri'
export const no_valid_roles_found = 'Nessun ruolo valido trovato. Digitare gli ID ruolo separati da ritorni a capo.'
export const you_can_only_add_up_to_25_roles_to_the_menu = 'Solo puoi aggiungere 25 ruoli al menu.'
export const invalid_roles_skipped = 'I ruoli non validi sono stati ignorati.'
export const manage_roles_permission_required = `Chrysalis ha bisogno dell'autorizzazione \`MANAGE_ROLES\` per far funzionare i menu dei ruoli.`
export const chrysalis_role_too_low = `Il ruolo de Chrysalis ha meno permessi che il ruolo richiesto. Per favore chiedi a un'Admin per menderlo.`
export const roles_managed_by_integrations_cannot_be_manually_assigned = 'I ruoli gestiti dalle integrazioni non possono essere assegnati manualmente.'
export const download_emoji = `Scarica l'emoji`
export const please_type_a_valid_channel = 'Per favore digita un canale valido.'
export const invalid_channel = 'Almeno un canale è invalido.'
export const invite_the_bot = 'Invita il bot'
export const website = 'Sito web'
export const support_server = 'Server di supporto'
export const source_code = 'Codice sorgente'
export const support_the_project = 'Sostenere il progetto'
export const unknown_role = 'Impossibile trovare il ruolo.'
export const role_id = 'ID del ruolo'
export const welcome = 'Benvenuto!'
export function you_are_the_member_n(n) { return `Sei membro Nº${n}` }
export const filter_not_found = 'Impossibile trovare il filtro, controlla se il filtro è pubblico.'
export const help_time_out = 'Il tempo per spostarsi tra le pagine è finito.'
export const old_message = 'Messagio vecchio'
export const new_message = 'Messagio nuovo'
export const old_attachments = 'Allegati vecchi'
export const new_attachments = 'Allegati nuovi'
export const message_edited = 'Messagio modificato'
export const season = 'Stagione'
export const seasons = 'Stagione'
export const movies = 'Film'
export const torrent_footer = 'Chrysalis consiglia di utilizzare qBittorrent in quanto è privo di pubblicità e open source.'
export const error_fetching_episodes = `C'era un errore durante il recupero dei link. Per favore riprova più tardi.`
export const attach_files_permission_missing = 'Chrysalis non ha permissi da inviare gli immagini in questo cannale.'
export const embed_links_permission_missing = `Chrysalis non ha il permesso di inviare embed a questo canale. Si prega di concedere a Chrysalis il permesso "Incorporare i link".`
export const please_type_a_valid_positive_integer = 'Por favore, invia un numero intero positivo.'
export const module_property_not_found = 'Proprietà del modulo non trovata.'
export function levelup(level) { return `**Sei salito di livello!**\nOra sei di livello **${level}**!` }
export const leaderboard_title = 'Classifica'
export const level = 'Livello'
export const rank = 'Rango'
export const total_xp = 'XP Totale'
export const profile = 'Profilo'
export const profile_updated = 'Profilo Aggiornato!'
export const unsupported_image_type = 'Tipo di immagine non supportato.'
export const check_documentation = 'Fare clic sul nome del modulo per aprire la documentazione per questo modulo.'
export const import_levels_from = 'Da quale bot vuoi importare i livelli?'
export function no_levels_found(bot) { return `Nessun livello trovato da ${bot} in questo server.` }
export function mee6_fix(guildID) { return `Assicurati la tua classifica MEE6 è pubblico cliccando [questa opzione](https://mee6.xyz/dashboard/${guildID}/leaderboard).` }
export const xp_migration_adapt = 'Il modo in cui viene calcolato XP potrebbe essere diverso da quello di altri bot. Cosa vuoi fare?'
export const import_levels_and_adapt_xp = 'Importa i livelli e adatta XP'
export const import_xp_and_adapt_levels = 'Importa il XP e adatta i livelli'
export const import_leaderboard = 'La classifica ora apparirà così:'
export const migration_complete = 'Migrazione completata!'
export const xp_successfully_imported = `L'XP è stato importato correttamente.`
export const edit = 'Modificare'
export const module_time_out = 'Il tempo per modificare questo modulo è finito'
export const use_modal_instead = 'Le parole bloccate non possono essere modificate in questo modo. Si prega di utilizzare il pulsante Modificare.'
export function messages_deleted(n) { return `${n} messaggi${n > 1 ? '' : 'o'} eliminat${n > 1 ? 'i' : 'o'}! ${PinkiePieYes}` }
export const optional = 'opzionale'
export const click_to_open_modal = 'Dal momento che non stai usando un comando slash, dovrai fare clic su questo pulsante per aprire il menu.'
export function captcha_attempts_left(n) { return `Sbagliato. Ti rimangono **${n}** tentativi.` }
export const verification = 'Verifica'

// Modal input field labels - Max 45 characters
export const enabled = 'Abilitato'
export const logDeletedMessages = 'Registra messaggi eliminati'
export const logEditedMessages = 'Registra messaggi modificati'
export const words = 'Parole/frasi (separati da ritorni a capo)'
export const title = 'Titolo'
export const description = 'Descrizione'
export const reactToLinks = 'Reagisci ai link'
export const reactToFiles = 'Reagisci ai file'
export const approvalEmoji = 'Emoji di approvazione'
export const disapprovalEmoji = 'Emoji di disapprovazione'
export const restricted = 'Comando limitato a determinati canali'
export const allowedChannels = 'Canali in cui è consentito il suo utilizzo'
export const filter = 'Filtro'
export const background = 'Sfondo'
export const banMessage = 'Messaggio di divieto'
export const xpBlacklistChannels = 'Canali in cui non è possibile guadagnare XP'
export const xpPerMessage = 'XP guadagnati per messaggio'
export const messageCooldown = 'XP cooldown sui messaggi (in secondi)'
export const xpInVoiceChat = 'XP guadagnati in VC (canale vocale)'
export const voiceChatCooldown = 'Quanto spesso guadagnare XP in VC (in sec)'
export const announceLevelUp = 'Annunciare i level up'
export const announceLevelUpChannel = 'Canale su cui annunciare i level up'
export const sendMessageOnDelete = 'Invia MD dopo aver eliminato il messaggio'
export const ignoreAdmins = 'Ingoria i messaggi degli admin'
export const role = 'Ruolo'
export const requiresCaptcha = 'Richiede captcha'
export const captchaChannel = 'Canale captcha'
export const captchaMaxAttempts = 'Captcha max tentativi'
export const captchaTimeout = 'Timeout Captcha'
export const roleBeforeCaptcha = 'Ruolo prima del captcha'
export const captchaDescription = 'Descrizione captcha'
export const captchaKickMessage = 'Messaggio captcha kick'
export const color = 'Colore'
export const background_image = 'Immagine di sfondo'
