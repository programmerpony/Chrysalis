/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { ApplicationCommandOptionType } from 'discord.js';
import defaultModules from '../defaultModules.js';
import { NitroBoost, PinkiePieYes } from '../emojis.js';

export const meta = {
  name: 'Español',
  new_lang_message: 'Ahora hablaré en español.'
}

/*
DO NOT TRANSLATE COMMAND NAMES, only descriptions, option names and option descriptions
Command description - Max 100 characters
Command option name - Max 32 chracters
Command option description - Max 100 characters
*/
export const commands = {
  user: [
    {
      name: 'avatar',
      description: 'Envía el avatar del usuario.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario del que enviar el avatar.'
        }
      ]
    },
    {
      name: 'userinfo',
      description: 'Muestra información sobre el usuario.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario del que mostrar la información.'
        }
      ]
    },
    {
      name: 'roleinfo',
      description: 'Envía información sobre el rol.',
      options: [
        {
          name: 'rol',
          type: ApplicationCommandOptionType.Role,
          description: 'Rol del que mostrar la información.',
          required: true
        }
      ]
    },
    {
      name: 'serverinfo',
      description: 'Muestra información sobre el servidor.'
    },
    {
      name: 'rank',
      description: 'Muestra tu nivel y experiencia actual.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario del que mostrar el nivel.'
        }
      ]
    },
    {
      name: 'leaderboard',
      description: 'Muestra los usuarios con más experiencia.'
    },
    {
      name: 'profile',
      description: 'Edita tu perfil (en todos los servidores).'
    },
    {
      name: 'emoji',
      description: 'Envía el emoji como una imagen.',
      options: [
        {
          name: 'emoji',
          type: ApplicationCommandOptionType.String,
          description: 'El emoji que quieres convertir en imagen.',
          required: true
        }
      ]
    },
    {
      name: 'love',
      description: 'Comprueba el nivel de compatibilidad entre dos usuarios.',
      options: [
        {
          name: 'amante1',
          type: ApplicationCommandOptionType.User,
          description: 'El primer miembro de la pareja.',
          required: true
        },
        {
          name: 'amante2',
          type: ApplicationCommandOptionType.User,
          description: 'El segundo miembro de la pareja.',
          required: true
        }
      ]
    },
    {
      name: 'boop',
      description: 'Hace boop a un usuario.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario al que quieres hacer boop.',
          required: true
        }
      ]
    },
    {
      name: 'hug',
      description: 'Abraza a un usuario.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario al que quieres abrazar.',
          required: true
        }
      ]
    },
    {
      name: 'kiss',
      description: 'Besa a un usuario.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario al que quieres besar.',
          required: true
        }
      ]
    },
    {
      name: 'booru',
      description: 'Envía una imagen de Manebooru.',
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags para buscar (separados por comas).'
        }
      ]
    },
    {
      name: 'clop',
      description: 'Envía una imagen clop de Manebooru.',
      nsfw: true,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags para buscar (separados por comas).'
        }
      ]
    },
    {
      name: 'e621',
      description: 'Envía una imagen de e621.',
      nsfw: true,
      options: [
        {
          name: 'tags',
          type: ApplicationCommandOptionType.String,
          description: 'Tags para buscar (separados por espacios).'
        }
      ]
    },
    {
      name: 'torrent',
      description: '¡Ponis por torrent!'
    },
    {
      name: 'say',
      description: 'Chrysalis dice lo que escribas.',
      options: [
        {
          name: 'texto',
          type: ApplicationCommandOptionType.String,
          description: 'Lo que quieres que Chrysalis diga.',
          required: true
        }
      ]
    }
  ],
  admin: [
    {
      name: 'config',
      description: `Edita los módulos de Chrysalis`,
      options: [
        {
          name: 'módulo',
          type: ApplicationCommandOptionType.String,
          description: 'Elige el módulo que quieras editar.',
          choices: defaultModules.map(m => { return { name: m.name, value: m.name } }),
          required: true
        }
      ]
    },
    {
      name: 'clean',
      description: 'Borra varios mensajes a la vez.',
      options: [
        {
          name: 'mensajes',
          type: ApplicationCommandOptionType.Integer,
          description: 'La cantidad de mensajes que quieres borrar.',
          minValue: 1,
          maxValue: 100,
          required: true
        }
      ]
    },
    {
      name: 'lang',
      description: 'Cambia el idioma de Chrysalis.',
      options: [
        {
          name: 'idioma',
          type: ApplicationCommandOptionType.String,
          description: 'El idioma en el que quieres que hable Chrysalis.',
          choices: [
            {
              name: 'Inglés',
              value: 'en'
            },
            {
              name: 'Español',
              value: 'es'
            },
            {
              name: 'Italiano',
              value: 'it'
            },
            {
              name: 'Francés',
              value: 'fr'
            }
          ],
          required: true
        }
      ]
    },
    {
      name: 'color',
      description: 'Cambia el color por defecto de los embeds.',
      options: [
        {
          name: 'color',
          type: ApplicationCommandOptionType.String,
          description: 'Debe ser un color hexadecimal.',
          maxLength: 7
        }
      ]
    },
    {
      name: 'welcome',
      description: 'Envía una tarjeta de bienvenida del usuario especificado.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario al que quieres dar la bienvenida.'
        }
      ]
    },
    {
      name: 'boost',
      description: 'Envía una tarjeta de boost del usuario especificado.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario del que quieres mostrar la tarjeta de boost.'
        }
      ]
    },
    {
      name: 'setxp',
      description: 'Establece la cantidad de XP del usuario especificado.',
      options: [
        {
          name: 'usuario',
          type: ApplicationCommandOptionType.User,
          description: 'El usuario al que quieres cambiar la XP.',
          required: true
        },
        {
          name: 'xp',
          type: ApplicationCommandOptionType.Integer,
          description: 'La nueva cantidad de XP.',
          minValue: 0,
          maxValue: Number.MAX_SAFE_INTEGER,
          required: true
        }
      ]
    },
    {
      name: 'importxp',
      description: 'Importa XP de otros bots de niveles.'
    },
    {
      name: 'rolemenu',
      description: 'Crea un menú para que los usuarios puedan elegir roles.',
      options: [
        {
          name: 'edit',
          type: ApplicationCommandOptionType.String,
          description: 'Si quieres modificar un menú ya existente, pega la URL del mensaje aquí.'
        }
      ]
    }
  ]
}
export const defaultValues = {
  bannedwords: {
    message: 'Se ha eliminado tu mensaje ya que estás tocando un tema que no permitimos en el servidor. Por favor, controla tu vocabulario y no trates de evadir la restricción. Esto se hace para mantener una buena convivencia.'
  },
  boost: {
    message: '**¡{user} ha mejorado el servidor!**',
    title: '¡Muchas gracias por boostear el servidor!',
    description: `${NitroBoost} ¡Disfruta de tu rol exclusivo! ${NitroBoost}`
  },
  welcome: {
    message: '¡Bienvenid@ a **{guild}**! {user}'
  },
  goodbye: {
    message: 'Adiós, **{user}**. Esperamos volver a verte pronto.',
    banMessage: '**{user}** ha sido banead@ del servidor.'
  },
  autorole: {
    captchaDescription: 'Resuelve este captcha para tener acceso a **{guild}**. Tienes {seconds} segundos antes de que se te expulse automáticamente.',
    captchaKickMessage: 'Se te ha expulsado de **{guild}** por no resolver el captcha.'
  }
}
export const author = 'Autor'
export const message = 'Mensaje'
export const channel = 'Canal'
export const user_commands = 'Comandos:'
export const admin_commands = 'Comandos de administradores:'
export const wrong_channel = 'El comando que has enviado no se puede usar en ese canal.'
export const message_deleted = 'Mensaje eliminado'
export const you_must_send_an_emoji = 'Debes enviar un emoji.'
export const couldn_t_find_that_emoji = 'No he podido encontrar ese emoji.'
export const type_one_or_two_users = 'Escribe uno o dos usuarios.'
export const self_love = 'Sé que el amor propio es importante, pero no seas narcisista.'
export const lovemeter_messages = [
  'Lamentable.',
  'Nada compatibles.',
  'Muy bajo... Solo amigos.',
  'Posiblemente queden como solo amigos.',
  'Muy poco probable pero puede que haya algo.',
  'Es probable que haya algo, pero no quiero dar ilusiones.',
  'Es bastante probable que haya algo. Se puede intentar.',
  'Bastante compatibles. Podrían ser una linda pareja.',
  'Estos dos están liados seguro.',
  'Pueden llegar a ser una hermosa pareja.',
  'Están destinados a estar juntos para siempre.'
]
export const nsfw_only = 'Este comando solo puede ser enviado a un canal NSFW.'
export const requested_by = 'Solicitado por'
export const how_to_delete = 'Pulsa aquí si la imagen es inapropiada.'
export const please_report = 'Por favor, reporta esta imagen para que no vuelva a aparecer.'
export const role_info = 'Información del rol:'
export const name = 'Nombre'
export const member_count = 'Número de miembros'
export const date_created = 'Fecha en la que fue creado'
export const couldn_t_find_that_user = 'No he podido encontrar a ese usuario.'
export const user_info = 'Información de usuario:'
export const user_id = 'ID de usuario'
export const server_join_date = 'Fecha de ingreso al servidor'
export const account_creation_date = 'Fecha de creación de la cuenta'
export const roles = 'Roles'
export function avatar(user) { return `Avatar de ${user}` }
export const bulk_delete_two_weeks = 'No se pueden eliminar mensajes enviados hace más de dos semanas.'
export const bulk_delete_missing_permissions = 'No tengo permiso para borrar mensajes en este canal.'
export const bulk_delete_max_100 = 'Solo puedes borar menos de 100 mensajes de golpe.'
export const no_images_found = 'No se ha podido encontrar ninguna imagen.'
export const jump_to_moment = 'Saltar al momento'
export function the_current_prefix_for_this_server_is(prefix) { return `El prefijo actual de este servidor es ${prefix}` }
export function change_prefix_to(prefix) { return `¿Cambiar el prefijo a **${prefix}**?` }
export function prefix_was_changed_to(prefix) { return `El prefijo fue cambiado a **${prefix}**` }
export const valid_modules = 'Módulos válidos'
export const available_languages = 'Idiomas disponibles'
export function boop_title(user) { return `${user[0]} le ha hecho boop a ${user[1]}` }
export function boop_self(user) { return `${user} se ha hecho boop a sí mism@` }
export function boop_chrysalis(user) { return `${user} trató de hacerle boop a Chrysalis` }
export function hug_title(user) { return `${user[0]} ha abrazado a ${user[1]}` }
export function hug_self(user) { return `${user} se ha abrazado a sí mism@` }
export function hug_chrysalis(user) { return `${user} trató de abrazar a Chrysalis` }
export function kiss_title(user) { return `${user[0]} le ha dado un beso a ${user[1]}` }
export function kiss_self(user) { return `${user} se ha besado a sí mism@` }
export function kiss_chrysalis(user) { return `${user} intentó besar a Chrysalis` }
export const server_info = 'Información del servidor:'
export const server_id = 'ID del servidor'
export const server_owner = 'Propietari@ del servidor'
export const channels = 'Canales'
export const server_boosts = 'Mejoras del servidor'
export const image_source = 'Fuente de la imagen'
export const please_specify_a_new_value = 'Por favor, especifica un nuevo valor.'
export const value_must_be_true_or_false = 'El valor debe ser **true** o **false**.' // Do NOT translate **true** and **false**
export const module_updated = '¡Módulo actualizado!'
export const attachments = 'Archivos adjuntos'
export const message_id = 'ID del mensaje'
export function module_enabled(m) { return `Módulo ${m} habilitado.` }
export function module_disabled(m) { return `Módulo ${m} deshabilitado.` }
export function module_already_enabled(m) { return `El módulo ${m} ya estaba habilitado.` }
export function module_already_disabled(m) { return `El módulo ${m} ya estaba deshabilitado.` }
export const current_color = 'Color actual'
export function change_color_to(hex) { return `¿Cambiar color a ${hex}?` }
export const invalid_color = 'Color no válido.'
export function color_was_changed_to(hex) { return `Se ha cambiado el color a ${hex}` }
export const role_menu = 'Menú de roles'
export const role_menu_not_found = 'No se ha podido encontrar un menú de roles en la URL especificada'
export const select_the_roles_that_you_want = 'Selecciona los roles que quieras'
export const no_valid_roles_found = 'No se han encontrado roles válidos. Escribe IDs de roles separadas por saltos de línea.'
export const you_can_only_add_up_to_25_roles_to_the_menu = 'Solo puedes añadir hasta 25 roles al menú.'
export const invalid_roles_skipped = 'Se han omitido los roles inválidos.'
export const manage_roles_permission_required = 'Chrysalis necesita el permiso `MANAGE_ROLES` para que los menús de roles funcionen.'
export const chrysalis_role_too_low = 'El rol de Chrysalis está por debajo del rol que estás solicitando. Por favor, contacta a un administrador para que lo arregle.'
export const roles_managed_by_integrations_cannot_be_manually_assigned = 'Los roles administrados por integraciones no pueden ser asignados manualmente.'
export const download_emoji = 'Descargar emoji'
export const please_type_a_valid_channel = 'Por favor, escribe un canal válido.'
export const invalid_channel = 'Al menos uno de los canales que has introducido es inválido.'
export const invite_the_bot = 'Invita al bot'
export const website = 'Página web'
export const support_server = 'Servidor de soporte'
export const source_code = 'Código fuente'
export const support_the_project = 'Apoya el proyecto'
export const unknown_role = 'No he podido encontrar ese rol.'
export const role_id = 'ID del rol'
export const welcome = '¡Bienvenid@!'
export function you_are_the_member_n(n) { return `Eres el miembro Nº${n}` }
export const filter_not_found = 'Filtro no encontrado. Por favor, asegúrate  de que el filtro es público.'
export const help_time_out = 'El tiempo para pasar las páginas se ha acabado.'
export const old_message = 'Mensaje viejo'
export const new_message = 'Mensaje nuevo'
export const old_attachments = 'Antiguos archivos adjuntos'
export const new_attachments = 'Nuevos archivos adjuntos'
export const message_edited = 'Mensaje editado'
export const season = 'Temporada'
export const seasons = 'Temporadas'
export const movies = 'Películas'
export const torrent_footer = 'Chrysalis recomienda usar qBittorrent, ya que no contiene anuncios y es software libre.'
export const error_fetching_episodes = 'No se han podido obtener los episodios en este momento. Por favor, inténtalo más tarde.'
export const attach_files_permission_missing = 'Chrysalis no tiene permiso para enviar imágenes a este canal.'
export const embed_links_permission_missing = 'Chrysalis no tiene permiso para enviar embeds a este canal. Por favor, dale a Chrysalis el permiso de "Insertar enlaces"'
export const please_type_a_valid_positive_integer = 'Por favor, escribe un número entero positivo.'
export const module_property_not_found = 'Propiedad del módulo no encontrada.'
export function levelup(level) { return `**¡Has subido de nivel!**\nAhora eres nivel **${level}**.` }
export const leaderboard_title = 'Tabla de clasificación'
export const level = 'Nivel'
export const rank = 'Rango'
export const total_xp = 'XP total'
export const profile = 'Perfil'
export const profile_updated = '¡Perfil actualizado!'
export const unsupported_image_type = 'Tipo de imagen no soportado.'
export const check_documentation = 'Haz clic en el nombre del módulo para abrir la documentación de este módulo.'
export const import_levels_from = '¿De qué bot quieres importar los niveles?'
export function no_levels_found(bot) { return `No se han encontrado niveles de ${bot} en este servidor.` }
export function mee6_fix(guildID) { return `Por favor, asegúrate de que tu tabla de clasificación de MEE6 es pública activando [esta opción](https://mee6.xyz/dashboard/${guildID}/leaderboard).` }
export const xp_migration_adapt = 'La forma en que Chrysalis calcula niveles puede ser distinta a la de otros bots. ¿Qué quieres hacer?'
export const import_levels_and_adapt_xp = 'Importar niveles y adaptar XP'
export const import_xp_and_adapt_levels = 'Importar XP y adaptar niveles'
export const import_leaderboard = 'La tabla de clasificacion quedará tal que así:'
export const migration_complete = '¡Transferencia completada!'
export const xp_successfully_imported = 'La XP se ha importado correctamente.'
export const edit = 'Editar'
export const module_time_out = 'El tiempo para editar este módulo se ha acabado.'
export const use_modal_instead = 'Las palabras bloqueadas no se pueden editar así. Por favor, utiliza el botón de Editar.'
export function messages_deleted(n) { return `¡${n} mensaje${n > 1 ? 's' : ''} eliminado${n > 1 ? 's' : ''}! ${PinkiePieYes}` }
export const optional = 'opcional'
export const click_to_open_modal = 'Debido a que no estás usando un comando de barra diagonal, tendrás que pulsar este botón para abrir el menú.'
export function captcha_attempts_left(n) { return `Incorrecto. Te queda${n == 1 ? '' : 'n'} **${n}** intento${n == 1 ? '' : 's'}.` }
export const verification = 'Verificación'

// Modal input field labels - Max 45 characters
export const enabled = 'Habilitado'
export const logDeletedMessages = 'Registrar mensajes borrados'
export const logEditedMessages = 'Registrar mensajes editados'
export const words = 'Palabras/frases (separar por saltos de línea)'
export const title = 'Título'
export const description = 'Descripción'
export const reactToLinks = 'Reaccionar a enlaces'
export const reactToFiles = 'Reaccionar a archivos'
export const approvalEmoji = 'Emoji de aprobación'
export const disapprovalEmoji = 'Emoji de desaprobación'
export const restricted = 'Comando restringido a ciertos canales'
export const allowedChannels = 'Canales en los que está permitido su uso'
export const filter = 'Filtro'
export const background = 'Fondo'
export const banMessage = 'Mensaje de ban'
export const xpBlacklistChannels = 'Canales donde no se puede ganar XP'
export const xpPerMessage = 'XP ganada por mensaje'
export const messageCooldown = 'Tiempo entre mensajes para ganar XP (en seg)'
export const xpInVoiceChat = 'XP ganada en VC (canales de voz)'
export const voiceChatCooldown = 'Cada cuántos segundos ganar XP en VC'
export const announceLevelUp = 'Anunciar subidas de nivel'
export const announceLevelUpChannel = 'Canal en el que anunciar subidas de nivel'
export const sendMessageOnDelete = 'Enviar MD después de borrar'
export const ignoreAdmins = 'Ignorar mensajes de admins'
export const role = 'Rol'
export const requiresCaptcha = 'Requiere captcha'
export const captchaChannel = 'Canal del captcha'
export const captchaMaxAttempts = 'Máximo de intentos'
export const captchaTimeout = 'Límite de tiempo'
export const roleBeforeCaptcha = 'Rol antes del captcha'
export const captchaDescription = 'Descripción del captcha'
export const captchaKickMessage = 'Mensaje de expulsión'
export const color = 'Color'
export const background_image = 'Imagen de fondo'
