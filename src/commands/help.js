/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { EmbedBuilder, ButtonBuilder, ActionRowBuilder, ButtonStyle, PermissionsBitField } from 'discord.js';

export const name = 'help';
export const alias = ['commands', 'ayuda', 'cmds'];
export async function run(client, message, command, args, lang, guildInfo) {

  let helpEmbed = [];
  let i = 0;
  for (let ch of client.commands.filter(c => !c.admin && c.name != 'help').map(c => c.name)) {
    if (guildInfo.modules.find((c) => c.name == ch)?.enabled || guildInfo.modules.find((c) => c.name == client.commands.get(ch)?.dependsOn)?.enabled) {
      if (helpEmbed[i]?.data.fields.length == 5) i++;
      helpEmbed[i] ??= new EmbedBuilder({
        title: `__**${lang.user_commands}**__`,
        color: guildInfo.color
      });
      ch = lang.commands.user.find(c => c.name === ch);
      helpEmbed[i].addFields({
        name: `\`${guildInfo.prefix}${ch.name}${ch.options?.map(o => ` {${o.name}${o.required ? '' : ` (${lang.optional})`}}`).join('') || ''}\`${(ch.nsfw ? ' :warning:' : '')}`,
        value: ch.description
      });
    }
  }

  if (helpEmbed.length > 1) {
    let leftButton = new ButtonBuilder({
      style: ButtonStyle.Secondary,
      label: '<',
      customId: 'left',
      disabled: true
    });
    let rightButton = new ButtonBuilder({
      style: ButtonStyle.Secondary,
      label: '>',
      customId: 'right'
    });
    let sentEmbed = await message.channel.send({ embeds: [helpEmbed[0]], components: [new ActionRowBuilder().addComponents([leftButton, rightButton])] }).catch(r => { });
    if (!sentEmbed) return;
    let filter = (interaction) => interaction.user.id === message.author.id;
    let collector = sentEmbed.createMessageComponentCollector({ filter, time: 120000 });
    let currentPage = 0;
    collector.on('collect', async (i) => {
      if (i.customId == 'left') {
        if (currentPage > 0) currentPage--;
        leftButton.setDisabled(currentPage == 0);
        rightButton.setDisabled(false);
      } else {
        if (currentPage < helpEmbed.length - 1) currentPage++;
        rightButton.setDisabled(currentPage == helpEmbed.length - 1);
        leftButton.setDisabled(false);
      }
      try {
        await i.update({
          embeds: [helpEmbed[currentPage]],
          components: [new ActionRowBuilder().addComponents([leftButton, rightButton])]
        });
      } catch (e) { }
    });
    collector.on('end', async (collected, reason) => {
      if (reason == 'time') {
        leftButton.setDisabled(true);
        rightButton.setDisabled(true);
        try {
          await sentEmbed.edit({
            embeds: [helpEmbed[currentPage].setFooter({ text: lang.help_time_out })],
            components: [new ActionRowBuilder().addComponents([leftButton, rightButton])]
          });
        } catch (e) { }
      }
    });
  } else message.channel.send({ embeds: [helpEmbed[0]] }).catch(r => { });

  if (message.member.permissions.has(PermissionsBitField.Flags.Administrator)) {
    let adminHelpEmbed = [];
    let i = 0;
    for (let ch of client.commands.filter(c => c.admin).map(c => lang.commands.admin.find(h => h.name === c.name)).filter(e => e !== undefined)) {
      if (adminHelpEmbed[i]?.data.fields.length == 5) i++;
      adminHelpEmbed[i] ??= new EmbedBuilder({
        title: `__**${lang.admin_commands}**__`,
        color: guildInfo.color
      });
      adminHelpEmbed[i].addFields({
        name: `\`${guildInfo.prefix}${ch.name}${ch.options?.map(o => ` {${o.name}${o.required ? '' : ` (${lang.optional})`}}`).join('') || ''}\``,
        value: ch.description
      });
    }
    let leftButton = new ButtonBuilder({
      style: ButtonStyle.Secondary,
      label: '<',
      customId: 'left',
      disabled: true
    });
    let rightButton = new ButtonBuilder({
      style: ButtonStyle.Secondary,
      label: '>',
      customId: 'right'
    });
    let sentEmbed = await message.channel.send({ embeds: [adminHelpEmbed[0]], components: [new ActionRowBuilder().addComponents([leftButton, rightButton])] }).catch(r => { });
    if (!sentEmbed) return;
    let filter = (interaction) => interaction.user.id === message.author.id;
    let collector = sentEmbed.createMessageComponentCollector({ filter, time: 120000 });
    let currentPage = 0;
    collector.on('collect', async (i) => {
      if (i.customId == 'left') {
        if (currentPage > 0) currentPage--;
        leftButton.setDisabled(currentPage == 0);
        rightButton.setDisabled(false);
      } else {
        if (currentPage < adminHelpEmbed.length - 1) currentPage++;
        rightButton.setDisabled(currentPage == adminHelpEmbed.length - 1);
        leftButton.setDisabled(false);
      }
      try {
        await i.update({
          embeds: [adminHelpEmbed[currentPage]],
          components: [new ActionRowBuilder().addComponents([leftButton, rightButton])]
        });
      } catch (e) { }
    });
    collector.on('end', async (collected, reason) => {
      if (reason == 'time') {
        leftButton.setDisabled(true);
        rightButton.setDisabled(true);
        try {
          await sentEmbed.edit({
            embeds: [adminHelpEmbed[currentPage].setFooter({ text: lang.help_time_out })],
            components: [new ActionRowBuilder().addComponents([leftButton, rightButton])]
          });
        } catch (e) { }
      }
    });
  }
}
