/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { ActionRowBuilder, ButtonBuilder, ButtonStyle } from 'discord.js';
import connectToDatabase from '../utils/connectToDatabase.js';
import reply from '../utils/reply.js';
import { PinkiePieYes, PinkiePieNo } from '../emojis.js';

export const name = 'prefix';
export const alias = ['setprefix', 'changeprefix', 'prefixset', 'set-prefix', 'change-prefix'];
export const admin = true;
export async function run(client, message, command, args, lang, guildInfo) {

  if (!args[0]) return message.channel.send(lang.the_current_prefix_for_this_server_is(`**${guildInfo.prefix}**`)).catch(r => { });

  reply(message, {
    content: lang.change_prefix_to(args[0]),
    components: [new ActionRowBuilder().addComponents([
      new ButtonBuilder({
        customId: `prefix-${args[0]}`,
        emoji: PinkiePieYes,
        style: ButtonStyle.Success
      }),
      new ButtonBuilder({
        customId: 'prefix-dismiss',
        emoji: PinkiePieNo,
        style: ButtonStyle.Danger
      })
    ])]
  }).then(confMsg => {
    let collector = confMsg.createMessageComponentCollector({ filter: (i) => i.member.id === message.member.id, time: 15000 });
    collector.on('collect', async (i) => {
      collector.stop(i.customId === 'prefix-dismiss' ? 'time' : 'new prefix!!!');
      if (i.customId === `prefix-${args[0]}`) {
        let db = await connectToDatabase();
        let guilds = db.db('chrysalis').collection('guilds');
        await guilds.updateOne({ id: message.guild.id }, { $set: { prefix: args[0] } });
        db.close();
        i.update({ content: lang.prefix_was_changed_to(args[0]), components: [] });
      }
    });
    collector.on('end', (collected, reason) => {
      if (reason == 'time') try {
        confMsg.delete();
        message.delete();
      } catch (e) { }
    });
  });
}