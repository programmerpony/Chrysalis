/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { ButtonBuilder, ActionRowBuilder, ButtonStyle, PermissionsBitField, ModalBuilder, TextInputBuilder, TextInputStyle } from 'discord.js';
import reply from '../utils/reply.js';
import { PinkiePieYes } from '../emojis.js';

export const name = 'rolemenu';
export const alias = ['role-menu'];
export const admin = true;
export const ephemeral = true;
export const modal = true;
export async function run(client, message, command, args, lang, guildInfo) {

  if (!message.guild.members.me.permissions.has(PermissionsBitField.Flags.ManageRoles)) return reply(message, { content: lang.manage_roles_permission_required, ephemeral: true }, true);

  // Edit already existing role menu
  if (args[0]) {
    let url = args[0].split('/');
    let channel = await message.guild.channels.fetch(url[5]).catch(r => { });
    if (channel?.id) args[0] = await channel.messages.fetch(url[6]).catch(r => { });
    if (args[0]?.author?.id !== client.user.id || !args[0]?.components?.[0]?.components?.[0]?.customId.startsWith('role-')) return reply(message, { content: lang.role_menu_not_found, ephemeral: true }, true);
  }

  let data = args[0]?.id ? await setUp(message, lang, {
    title: args[0].embeds?.[0]?.title,
    description: args[0].embeds?.[0]?.description,
    roles: args[0].components.map(row => row.components.map(r => r.customId.slice(5)).join('\n')).join('\n')
  }) : await setUp(message, lang);
  if (!data) return;

  await data.interaction.deferReply({ ephemeral: true });

  let roles = [];
  await message.guild.roles.fetch();
  let skipped = false;
  for (let role of [...new Set(data.roles)]) try {
    roles.push(message.guild.roles.cache.get(role).id);
  } catch (e) { skipped = true; }

  if (roles.length == 0) return data.interaction.editReply({ content: lang.no_valid_roles_found });
  if (roles.length > 25) return data.interaction.editReply({ content: lang.you_can_only_add_up_to_25_roles_to_the_menu });

  let rows = [];
  rows[0] = roles.slice(0, 5);
  rows[1] = roles.slice(5, 10);
  rows[2] = roles.slice(10, 15);
  rows[3] = roles.slice(15, 20);
  rows[4] = roles.slice(20, 25);

  let buttonRow = [];

  for (let [index, row] of rows.entries()) {
    if (row.length == 0) break;
    buttonRow[index] = new ActionRowBuilder();
    for (let role of row) {
      buttonRow[index].addComponents(new ButtonBuilder({
        style: ButtonStyle.Primary,
        label: message.guild.roles.cache.get(role).name,
        customId: `role-${role}`
      }));
    }
  }

  let title = data.title || lang.role_menu;
  let description = data.description || lang.select_the_roles_that_you_want;
  let menu = {
    embeds: !title && !description ? null : [{
      title: title,
      description: description,
      color: guildInfo.color
    }], components: buttonRow
  };
  if (args[0]?.id) args[0].edit(menu).catch(r => { });
  else message.channel.send(menu).catch(r => { });

  data.interaction.editReply({ content: skipped ? lang.invalid_roles_skipped : PinkiePieYes });

}

async function setUp(i, lang, edit) {

  if (i.author) {
    let button = await reply(i, {
      content: lang.click_to_open_modal,
      components:[new ActionRowBuilder().addComponents(new ButtonBuilder({
        customId: 'rolemenu-button',
        label: lang.role_menu,
        style: ButtonStyle.Primary
    }))]}, true);
    return await button.awaitMessageComponent({filter: b => b.user.id === i.author.id && b.customId === 'rolemenu-button', time: 12_000})
      .then(b=>setUp(b, lang, edit))
      .catch(async () => {
        try {
          await button.delete();
          await i.delete();
        } catch (e) {}
      });
  }

  await i.showModal(new ModalBuilder({
    customId: `rolemenu-${i.id}`,
    title: lang.role_menu,
    components: [
      new ActionRowBuilder().addComponents(
        new TextInputBuilder({
          customId: 'title',
          label: lang.title,
          style: TextInputStyle.Short,
          value: edit?.title || lang.role_menu,
          required: false
        })
      ),
      new ActionRowBuilder().addComponents(
        new TextInputBuilder({
          customId: 'description',
          label: lang.description,
          style: TextInputStyle.Paragraph,
          value: edit?.description || lang.select_the_roles_that_you_want,
          required: false
        })
      ),
      new ActionRowBuilder().addComponents(
        new TextInputBuilder({
          customId: 'roles',
          label: lang.roles,
          style: TextInputStyle.Paragraph,
          value: edit?.roles || '',
          required: true
        })
      )
    ]
  }));
  let answer = await i.awaitModalSubmit({ filter: a => a.customId === `rolemenu-${i.id}`, time: 120_000 }).catch(r=>{});
  if (!answer) return;
  return {
    title: answer.fields.getTextInputValue('title'),
    description: answer.fields.getTextInputValue('description'),
    roles: answer.fields.getTextInputValue('roles')
      .replaceAll(' ','\n')
      .replaceAll(',','\n')
      .replaceAll('<','\n')
      .replaceAll('@','\n')
      .replaceAll('>','\n')
      .split('\n')
      .filter(e=>e!==''),
    interaction: answer
  };

}