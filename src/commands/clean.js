/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { PermissionsBitField } from "discord.js";
import reply from '../utils/reply.js';

export const name = 'clean';
export const alias = ['bulkdelete', 'bulk-delete', 'clear', 'purge'];
export const admin = true;
export const ephemeral = true;
export async function run(client, message, command, args, lang, guildInfo) {
  // Check if Chrysalis has permission to delete messages
  if (!message.channel.permissionsFor(client.user.id).has(PermissionsBitField.Flags.ManageMessages)) {
    return reply(message, { content: lang.bulk_delete_missing_permissions }, true);
  }

  if (!isNaN(args[0])) {
    let messagesToDelete = parseInt(args[0]);
    if (message.author) messagesToDelete++;
    if (messagesToDelete <= 100) {
      try {
        await message.channel.bulkDelete(messagesToDelete);
      } catch (e) {
        return reply(message, { content: lang.bulk_delete_two_weeks }, true);
      }
    } else {
      return reply(message, { content: lang.bulk_delete_max_100 }, true);
    }
    if (!message.author) message.editReply(lang.messages_deleted(messagesToDelete));
  }
}
