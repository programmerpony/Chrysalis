/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import boost from '../modules/boost.js';
import mention2id from '../utils/mention2id.js';

export const name = 'boost';
export const alias = ['nitro'];
export const admin = true;
export async function run(client, message, command, args, lang, guildInfo) {

  boost(
    await args[0] ? await message.guild.members.fetch(mention2id(args[0])).catch(r => { }) : null || message.member,
    guildInfo,
    message.channel,
    message.author ? null : message,
    lang
  );

}
