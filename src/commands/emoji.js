/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import reply from '../utils/reply.js';

export const name = 'emoji';
export const alias = ['jumbo'];
export async function run(client, message, command, args, lang, guildInfo) {

  if (!args[0]) return reply(message, { content: lang.couldn_t_find_that_emoji }, true);

  const id = args[0].split(':')[2]?.replace('>', '') || args[0];
  const emoji = id.startsWith(':') && id.endsWith(':') ? client.emojis.cache.find(e => e.name == id.slice(1, -1)) : client.emojis.cache.get(id);
  const options = { extension: args[0].startsWith('<:') ? 'png' : 'gif' };
  let image = emoji?.imageURL(options) || client.rest.cdn.emoji(id, options);

  // If only the ID is provided, check if it's animated first
  if (id == args[0]) {
    let gif = await fetch(image);
    if (!gif.ok) image = `${image.slice(0, -3)}png`;
  }

  // Check if the image actually exists
  await fetch(image).then(res => res.ok ?
    reply(message, {
      embeds: [{
        title: lang.download_emoji,
        url: image,
        color: guildInfo.color,
        image: { url: image }
      }]
    }) :
    reply(message, { content: lang.couldn_t_find_that_emoji }, true)
  );

}
