/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import mention2id from '../utils/mention2id.js';
import reply from '../utils/reply.js';

export const name = 'avatar';
export const alias = ['pfp'];
export async function run(client, message, command, args, lang, guildInfo) {

  let taggedUser = mention2id(args[0]) || message.member.user.id;
  try {
    taggedUser = await message.guild.members.fetch(taggedUser);
  } catch (e) {
    try {
      taggedUser = await client.users.fetch(taggedUser);
    } catch (e) {
      return reply(message, { content: lang.couldn_t_find_that_user }, true);
    }
  }


  // If tagged user is Chrysalis, send profile picture artwork source
  if (taggedUser.id == client.user.id) return reply(message, 'https://www.deviantart.com/mirroredsea/art/Chrysalis-718716441');

  return reply(message, {
    embeds: [{
      title: lang.avatar(taggedUser.displayName || taggedUser.username),
      image: { url: taggedUser.displayAvatarURL({ size: 4096 }) },
      color: taggedUser.displayColor || guildInfo.color
    }]
  });

}
