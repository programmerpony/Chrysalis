/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { AttachmentBuilder } from 'discord.js';
import { createCanvas, loadImage } from 'canvas';
import connectToDatabase from '../utils/connectToDatabase.js';
import mention2id from '../utils/mention2id.js';
import refreshAttachment from '../utils/refreshAttachment.js';
import reply from '../utils/reply.js';

export const name = 'rank';
export const alias = ['level', 'r'];
export async function run(client, message, command, args, lang, guildInfo) {

  const taggedUser = mention2id(args[0]) || message.member.user.id;

  try {
    const taggedUserObject = await client.users.fetch(taggedUser); // Check if it's a valid user
    const rank = guildInfo.modules.find((c) => c.name == 'rank');
    let user = rank.users.find(u => u.id == taggedUser);
    if (!user) {
      rank.users.push({ id: taggedUser, xp: 0 });
      user = rank.users.find(u => u.id == taggedUser);
    }
    const userLevel = Math.trunc((Math.sqrt(5) / 5) * Math.sqrt(user.xp));
    const highscores = rank.users.sort((a, b) => (a.xp < b.xp) ? 1 : -1);

    const db = await connectToDatabase();
    const users = db.db('chrysalis').collection('users');
    const userPrefs = await users.findOne({ id: user.id });
    db.close();

    await rankCard(
      taggedUserObject,
      userPrefs?.color || `#${guildInfo.color.toString(16)}`,
      userPrefs?.bgURL,
      highscores.indexOf(user) + 1, // rank
      userLevel, // level
      user.xp - (userLevel * userLevel * 5), // currentXP
      ((userLevel + 1) * (userLevel + 1) * 5) - (userLevel * userLevel * 5), // requiredXP
      user.xp, // totalXP
      message,
      lang
    );

  } catch (e) {
    reply(message, lang.couldn_t_find_that_user, true);
  }
}

async function rankCard(user, color, bgURL, rank, level, currentXP, requiredXP, totalXP, message, lang) {

  // Create canvas
  const canvas = createCanvas(934,282);
  const ctx = canvas.getContext('2d');

  // Background
  ctx.fillStyle = '#23272a';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = '#333640';
  ctx.globalAlpha = 0.5;
  ctx.fillRect(10, 10, canvas.width - 20, canvas.height - 20);
  ctx.globalAlpha = 1;
  if (bgURL) try {
    bgURL = await refreshAttachment(bgURL);
    const bg = await loadImage(bgURL);
    ctx.drawImage(bg, 10, 10, canvas.width - 20, canvas.height - 20);
    ctx.shadowColor = 'rgba(0,0,0,1)';
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.shadowBlur = 10;
  } catch (e) {/* Image URL is invalid */}

  // Text
  ctx.font = '42px Montserrat Black';
  ctx.textAlign = 'left';
	ctx.fillStyle = 'white';
  // Username
  const usrtxt = user.username.length > 16 ? user.username.slice(0,14)+'...' : user.username;
  ctx.fillText(`${usrtxt}`, canvas.width/2-200, canvas.height/2);
  ctx.font = '36px Montserrat Medium';
  if (user.discriminator !== '0') {
    ctx.globalAlpha = 0.5;
    ctx.fillText(`#${user.discriminator}`, canvas.width/2-200 + ctx.measureText(usrtxt).width, canvas.height/2);
    ctx.globalAlpha = 1;
  }
  ctx.textAlign = 'right';
  ctx.fillStyle = 'white';
  // Rank and level
  ctx.fillText(`${lang.level.toUpperCase()}`, canvas.width-50-ctx.measureText(` ${level}`).width, 75);
  ctx.fillStyle = color;
  ctx.fillText(`${level}`, canvas.width-50, 75);
  ctx.fillStyle = 'white';
  ctx.fillText(`${lang.rank.toUpperCase()}`, canvas.width-ctx.measureText(`${lang.level.toUpperCase()} ${level}`).width-75-ctx.measureText(` #${rank}`).width, 75);
  ctx.fillStyle = color;
  ctx.fillText(`#${rank}`, canvas.width-ctx.measureText(`${lang.level.toUpperCase()} ${level}`).width-75, 75);
  ctx.fillStyle = 'white';
  ctx.font = '28px Montserrat Medium';
  // Current and required XP
  ctx.fillText(`${currentXP}/${requiredXP}`, canvas.width-50, canvas.height/2);
  ctx.font = '24px Montserrat Medium';
  ctx.fillText(`${lang.total_xp.toUpperCase()}: ${totalXP}`, canvas.width-50, canvas.height/2+85);

  // Progress bar
  const lineX = 260;
  const lineY = 155;
  const lineHeight = 38;
  const lineWidth = 620;
  const r = lineHeight / 2; // Arc radius
  const canvas2 = createCanvas(934,282); // Create another canvas so that the elements merge before applying the transparency
  const ctx2 = canvas2.getContext('2d');
  ctx2.fillStyle = '#151515';
  ctx2.arc(lineX + r, lineY + r, r, 1.5 * Math.PI, 0.5 * Math.PI, true);
  ctx2.fill();
  ctx2.fillRect(lineX + r, lineY, lineWidth - r, lineHeight);
  ctx2.arc(lineX + lineWidth, lineY + r, r, 1.5 * Math.PI, 0.5 * Math.PI);
  ctx2.fill();
  ctx2.beginPath();
  ctx.shadowOffsetX = 0;
  ctx.shadowOffsetY = 0;
  ctx.shadowBlur = 0;
  ctx.globalAlpha = 0.75;
  ctx.drawImage(canvas2,0,0,canvas.width,canvas.height);
  ctx.globalAlpha = 1;
  ctx.fillStyle = color;
  ctx.arc(lineX + r, lineY + r, r, 1.5 * Math.PI, 0.5 * Math.PI, true);
  ctx.fill();
  const progress = getProgress(currentXP, requiredXP, lineWidth, r);
  ctx.fillRect(lineX + r, lineY, progress, lineHeight);
  ctx.arc(lineX + r + progress, lineY + r, r, 1.5 * Math.PI, 0.5 * Math.PI);
  ctx.fill();

  // Profile picture
  const radius = 100;
  const x = 135;
  ctx.beginPath();
  ctx.arc(x, canvas.height/2, radius, 0, Math.PI * 2, true);
  ctx.strokeStyle = color;
  ctx.lineWidth = '15';
  ctx.stroke();
  ctx.closePath();
  ctx.clip();
  const avatar = user.displayAvatarURL({ extension: 'png', size: 1024 });
  try {
    const pfp = await loadImage(avatar);
    ctx.drawImage(pfp, x-radius, canvas.height/2-radius, radius*2, radius*2);
  } catch (e) { /* Avatar couldn't be fetched. The avatar will be completely transparent. */ }

  // Send the image
  reply(message, {files:[new AttachmentBuilder(canvas.toBuffer(), {
    name: 'rank.png',
    description: `${lang.name}: ${user.username}. ${lang.rank}: ${rank}. ${lang.level}: ${level}. ${lang.total_xp}: ${totalXP}.`
  })]});

}

function getProgress(currentXP, requiredXP, lineWidth, r) {
  const maxWidth = lineWidth - r;
  const width = (currentXP * lineWidth) / requiredXP;
  if (currentXP > requiredXP || width > maxWidth) return maxWidth;
  return width;
}
