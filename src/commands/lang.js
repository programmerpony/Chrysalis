/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import reloadSlashCommands from '../utils/reloadSlashCommands.js';
import connectToDatabase from '../utils/connectToDatabase.js';
import path from 'path';
import fs from 'fs';
import reply from '../utils/reply.js';

export const validLangs = fs.readdirSync(path.resolve(import.meta.dirname, '../lang/'))
                    .filter(f => f.endsWith('.js'))
                    .map(f => f.slice(0,-3));

export const name = 'lang';
export const alias = ['setlang','language','setlanguage'];
export const admin = true;
export async function run(client, message, command, args, lang, guildInfo) {
  if (validLangs.indexOf(args[0])>-1) return changeLang(client, message, args[0]);
  message.channel.send({embeds:[{
    title: lang.available_languages,
    description: validLangs.map(l =>`${l} (${lang.commands.admin.find(c=>c.name=='lang').options[0].choices.find(c=>c.value==l).name})`).join('\n'),
    color: guildInfo.color
  }]}).catch(r=>{});
}

async function changeLang(client, message, newLang) {
  let db = await connectToDatabase();
  let guilds = db.db('chrysalis').collection('guilds');
  let guild = await guilds.findOne({id: message.guild.id});
  await guilds.updateOne(guild,{ $set: { lang: newLang}});
  guild.lang = newLang;
  db.close();
  reloadSlashCommands(client, message.guild, guild);
  let newLangObject = await import(`../lang/${newLang}.js`);
  reply(message, { content: newLangObject.meta.new_lang_message });
}
