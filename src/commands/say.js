/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { PermissionsBitField } from 'discord.js';
import { PinkiePieNo, PinkiePieYes } from '../emojis.js';

export const name = 'say';
export const alias = ['text'];
export const ephemeral = true;
export async function run(client, message, command, args, lang, guildInfo) {

  // Check banned words
  if (!message.author) {
    const bannedwords = guildInfo.modules.find(m => m.name == 'bannedwords');
    if (bannedwords.enabled) {
      if (!bannedwords.ignoreAdmins || !message.member.permissions.has(PermissionsBitField.Flags.Administrator)) {
        for (const word of bannedwords.words) {
          if (args[0].includes(word)) {
            return message.editReply({content: PinkiePieNo});
          }
        }
      }
    }
  }

  const text = args.join(' ') || '_ _';
    
  if (message.author) {
    message.delete().catch(r=>{});
    if (message.reference?.messageId) {
      let msg = await message.channel.messages.fetch(message.reference.messageId).catch(r=>{});
      if (msg) return msg.reply(text).catch(r=>{});
    }
    message.channel.send(text).catch(r=>{});
  } else {
    message.editReply({content:PinkiePieYes});
    const say = await message.channel.send(text).catch(r=>{return;});
    const logs = guildInfo.modules.find(m => m.name == 'logs');
    if (logs.enabled && logs.channel) {
      const channel = await message.guild.channels.fetch(logs.channel).catch(r=>{});
      channel.send({embeds:[{
        title: 'c!say',
        color: guildInfo.color,
        author: { name: message.member.user.tag, iconURL: message.member.user.displayAvatarURL() },
        fields: [
          { name: lang.author,  value: `<@!${message.member.id}>` },
          { name: lang.message, value: text },
          { name: lang.message_id, value: say.id },
          { name: lang.channel, value: `${say.channel} [${lang.jump_to_moment}](${say.url})` }
        ]
      }]});
    }
  }

}
