/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { TextInputBuilder, ActionRowBuilder, ButtonBuilder, TextInputStyle, ButtonStyle, ModalBuilder, resolveColor } from 'discord.js';
import { loadImage } from 'canvas';
import connectToDatabase from '../utils/connectToDatabase.js';
import refreshAttachment from '../utils/refreshAttachment.js';
import reply from '../utils/reply.js';

export const name = 'profile';
export const alias = ['profile', 'editprofile'];
export const modal = true;
export async function run(client, message, command, args, lang, guildInfo) {

  if (!message.author) return showModal(guildInfo, message, lang);

  let button = await reply(message, {
    content: lang.click_to_open_modal,
    components: [new ActionRowBuilder().addComponents(new ButtonBuilder({
      customId: 'profile-button',
      label: lang.profile,
      style: ButtonStyle.Primary
    }))]
  }, true);
  button.awaitMessageComponent({ filter: (i) => i.user.id === message.author.id && i.customId === 'profile-button', time: 12000 })
    .then(i => showModal(guildInfo, i, lang, button, message))
    .catch(async () => {
      try {
        await button.delete();
        await message.delete();
      } catch (e) { }
    });

}

async function showModal(guildInfo, i, lang, button, message) {
  let data = await getData(i.member.id);
  await i.showModal(new ModalBuilder({
    customId: `profile-${i.member.id}`,
    title: lang.profile,
    components: [
      new ActionRowBuilder().addComponents(
        new TextInputBuilder({
          customId: 'color',
          label: lang.color,
          style: TextInputStyle.Short,
          value: data.color,
          placeholder: data.color?.slice(0,7),
          required: false
        })
      ),
      new ActionRowBuilder().addComponents(
        new TextInputBuilder({
          customId: 'bgURL',
          label: lang.background_image,
          style: TextInputStyle.Short,
          value: data.bgURL,
          placeholder: data.bgURL?.slice(0,100),
          required: false
        })
      )
    ]
  }));
  let filter = (interaction) => interaction.customId === `profile-${i.member.id}`;
  i.awaitModalSubmit({ filter, time: 120_000 })
    .then(async (answer) => {

      await answer.deferReply();

      if (button && message) try {
        await button.delete();
        await message.delete();
      } catch (e) {}

      let newColor = answer.fields.getTextInputValue('color');
      let newBG = answer.fields.getTextInputValue('bgURL');
      
      if (newColor) {
        newColor = `#${newColor.replaceAll('#','').repeat(6).slice(0,6)}`;
        try {
          resolveColor(newColor);
        } catch (e) {
          return answer.editReply({content:lang.invalid_color, ephemeral:true});
        }
      }
  
      if (newBG) {
        newBG =await refreshAttachment(newBG);
        try {
          await loadImage(newBG);
        } catch (e) {
          return answer.editReply({content:lang.unsupported_image_type, ephemeral:true});
        }
      }
  
      if (!newColor && !newBG) return answer.editReply({content:lang.please_specify_a_new_value, ephemeral:true});

      let db = await connectToDatabase();
      let users = db.db('chrysalis').collection('users');
      let userPrefs = await users.findOne({id:i.member.id});
      if (newColor) await users.updateOne({id: i.member.id},{ $set: { color: newColor}});
      if (newBG) await users.updateOne({id: i.member.id},{ $set: { bgURL: newBG}});
      db.close();

      return answer.editReply({embeds:[{
        title: lang.profile_updated,
        color: resolveColor(newColor || userPrefs.color || guildInfo.color),
        image: { url: newBG || userPrefs.bgURL }
      }], ephemeral:true});

    }).catch(r => {});
}

async function getData(id) {
  let db = await connectToDatabase();
  let users = db.db('chrysalis').collection('users');
  let userPrefs = await users.findOne({id:id});
  if (!userPrefs) {
    await users.insertOne({id:id});
    userPrefs = await users.findOne({id:id});
  }
  db.close();
  return userPrefs;
}