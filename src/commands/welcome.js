/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { PermissionsBitField } from 'discord.js';
import welcomeCard from '../modules/welcome.js';
import mention2id from '../utils/mention2id.js';
import reply from '../utils/reply.js';

export const name = 'welcome';
export const alias = ['welcome-card', 'welcome-image', 'greeting', 'greeting-image', 'greeting-card'];
export const admin = true;
export async function run(client, message, command, args, lang, guildInfo) {

  if (!message.channel.permissionsFor(client.user.id).has(PermissionsBitField.Flags.AttachFiles)) return reply(message, { content: lang.attach_files_permission_missing }, true);

  let user = args[0] ? await client.users.fetch(mention2id(args[0])).catch(r => { }) : null || message.member.user;
  user ? await welcomeCard(
    guildInfo,
    message.guild,
    lang,
    user,
    message.author ? null : message,
    message.channel
  ) : reply(message, { content: lang.couldn_t_find_that_user }, true);
}
