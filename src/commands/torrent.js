/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { EmbedBuilder, ActionRowBuilder, StringSelectMenuBuilder } from 'discord.js';
import rss from 'rss-to-json';
import reply from '../utils/reply.js';
const qBittorrent = 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/New_qBittorrent_Logo.svg/240px-New_qBittorrent_Logo.svg.png'
const yayponies = 'https://yayponies.no/favicon-32x32.png';

export const name = 'torrent';
export const alias = ['torrents', 'episode', 'episodes', 'mylittlepony', 'mlp', 'fim', 'mlpfim', 'download', 'movie', 'movies'];
export async function run(client, message, command, args, lang, guildInfo) {

  try { // Just in case yayponies is down
    const season = [];
    const feed = await rss.parse('https://yayponies.no/videos/rss/1it.rss');
    feed.items.forEach(item => {
      // Add episodes to seasons
      const currentSeason = +item.title.slice(item.title.indexOf('0'), item.title.indexOf('0') + 2);
      const currentEpisode = +item.title.slice(item.title.indexOf('x') + 1, item.title.indexOf('x') + 3);
      season[currentSeason] ??= { episode: [] };
      // Trixie is best pony
      season[currentSeason].episode[currentEpisode] = {
        title: item.title.slice(item.title.indexOf('0'), item.title.indexOf('|')),
        link: item.link
      };
    });

    // Create season embeds
    const seasonEmbed = [];
    for (const s in season) {
      if (s === '0') continue;
      seasonEmbed[s] = new EmbedBuilder()
        .setTitle(`${lang.season} ${s}`)
        .setAuthor({ name: 'yayponies.no', url: 'https://yayponies.no/', iconURL: yayponies })
        .setColor(guildInfo.color)
        .setFooter({ text: lang.torrent_footer, iconURL: qBittorrent });
      let episodes = [];
      for (const e of season[s].episode) {
        if (!e) continue;
        episodes += `[${e.title}](${e.link})\n`;
      }
      episodes += `[Subtitles in English](https://yayponies.no/subtitles/subs/YP-SUBS-REG-0${s}.zip)`;
      seasonEmbed[s].setDescription(episodes);
    }

    // Select menu
    const menu = new StringSelectMenuBuilder()
      .setCustomId('torrent')
      .setPlaceholder(`${lang.seasons}:`); // For mobile
    for (const s in season) {
      if (s === '0') continue;
      menu.addOptions({
        label: `${lang.season} ${s}`,
        value: s,
        default: s === '1'
      });
    }

    // Add movies too
    seasonEmbed[10] = new EmbedBuilder()
      .setTitle(lang.movies)
      .setAuthor({ name: 'yayponies.no', url: 'https://yayponies.no/', iconURL: yayponies })
      .setColor(guildInfo.color)
      .setFooter({ text: lang.torrent_footer, iconURL: qBittorrent })
      .setDescription('[My Little Pony: The Movie](https://yayponies.no/videos/torrents/YP-1R-TheMovie.mkv.torrent)\n[My Little Pony: A New Generation](https://yayponies.no/videos/torrents/YP-1N-G5-ANewGeneration.mkv.torrent)');
    menu.addOptions({
      label: lang.movies,
      value: '10'
    });

    // Send the message
    const m = await reply(message, { embeds: [seasonEmbed[1]], components: [new ActionRowBuilder().addComponents([menu])] });

    // Select menu collector
    const filter = (interaction) => interaction.user.id === message.member.user.id;
    const collector = m.createMessageComponentCollector({ filter, time: 120000 });
    let index = 1;
    collector.on('collect', async (i) => {
      if (i.customId !== 'torrent') return;
      if (index == i.values[0]) return await m.edit({}).catch(r => { }); // You can re-select the already selected option on the mobile app for some reason
      menu.options.find(o => o.data.value == index).data.default = false;
      index = i.values[0];
      menu.options.find(o => o.data.value == index).data.default = true;
      await m.edit({ embeds: [seasonEmbed[index]], components: [new ActionRowBuilder().addComponents([menu])] }).then(i.deferUpdate()).catch(r => { });
    });
    collector.on('end', async (collected, reason) => {
      if (reason == 'time') {
        await m.edit({ embeds: [seasonEmbed[index].setFooter({ text: `${seasonEmbed[index].data.footer.text}\n${lang.help_time_out}`, iconURL: qBittorrent })], components: [] }).catch(r => { });
      }
    });
  } catch (e) { return reply(message, { content: lang.error_fetching_episodes }, true); }
}
