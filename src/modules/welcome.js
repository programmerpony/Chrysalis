/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { AttachmentBuilder, PermissionsBitField } from 'discord.js';
import { createCanvas, loadImage } from 'canvas';
import refreshAttachment from '../utils/refreshAttachment.js';

export default async (guildInfo, guild, lang, user, i, channel) => {

    // Check if image can be sent
    const welcome = guildInfo.modules.find(c => c.name == 'welcome');
	if (!welcome.enabled) return;
    if (!channel && !welcome.channel) return;
	channel ??= await guild.channels.fetch(welcome.channel).catch(r=>{});
	if (!channel) return;
	if (!channel.permissionsFor(guild.client.user.id).has(PermissionsBitField.Flags.ViewChannel)) return;
	if (!channel.permissionsFor(guild.client.user.id).has(PermissionsBitField.Flags.SendMessages)) return;
	if (!channel.permissionsFor(guild.client.user.id).has(PermissionsBitField.Flags.AttachFiles)) return;

    // Create canvas
    const canvas = createCanvas(960,540);
    const ctx = canvas.getContext('2d');

    // Set background image (if any)
    if (welcome.background) {
        try {
            const bg = await loadImage(await refreshAttachment(welcome.background));
            ctx.drawImage(bg, 0, 0, canvas.width, canvas.height);
        } catch (e) { /* Image URL is invalid */ }
    }

    // Text
    ctx.font = '96px Montserrat Black';
    ctx.textAlign = 'center';
    ctx.fillStyle = 'white';
    ctx.shadowColor = 'rgba(0,0,0,1)';
    ctx.shadowOffsetX = 2;
    ctx.shadowOffsetY = 2;
    ctx.shadowBlur = 10;
    // Welcome
    ctx.fillText(lang.welcome.toUpperCase(), canvas.width/2, canvas.height/2+136);
    ctx.font = '48px Montserrat Black';
    // Username
    ctx.fillText(`${user.username.length > 21 ? user.username.toUpperCase().slice(0,18)+'...' : user.username.toUpperCase()}`, canvas.width/2, canvas.height/2+182);
    ctx.font = '36px Montserrat Black';
    // Member count
    ctx.fillText(lang.you_are_the_member_n(guild.memberCount).toUpperCase(), canvas.width/2, canvas.height/2+220);

    // Profile picture
    const radius = 128;
    ctx.beginPath();
    ctx.arc(canvas.width/2, canvas.height/2-80, radius, 0, Math.PI * 2, true);
    ctx.strokeStyle = 'white';
    ctx.lineWidth = '15';
    ctx.stroke();
    ctx.closePath();
    ctx.clip();
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.shadowBlur = 0;
    try {
        const pfp = await loadImage(user.displayAvatarURL({ extension: 'png', size: 1024 }));
        ctx.drawImage(pfp, canvas.width/2-radius, canvas.height/2-radius-80, radius*2, radius*2);
    } catch (e) { /* Avatar couldn't be fetched. The avatar will be completely transparent. */ }

    // Send the image
    let message;
    const attachment = new AttachmentBuilder(canvas.toBuffer(), {
        name: 'welcome.png',
        description: `${lang.welcome} ${user.username} ${lang.you_are_the_member_n(guild.memberCount)}`
    });
    if (['','...','off','none','null','false'].indexOf(welcome.message.trim()) > -1) {
        message = {files: [attachment]};
    } else {
        if (welcome.message == 'default') welcome.message = lang.defaultValues.welcome.message;
        message = {
            content: welcome.message
                .replaceAll('{user}',user)
                .replaceAll('{guild}',channel.guild.name),
            files: [attachment]
        }
    }
    return i ? i.editReply(message) : channel.send(message);
}