/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { PermissionsBitField } from "discord.js";

export default async (guildInfo, member) => {
    // Check if module is enabled
	const goodbye = guildInfo.modules.find((c) => c.name == 'goodbye');
	if (!goodbye.enabled) return;

    // Check if channel exists and Chrysalis has permission to send messages in it
	const channel = await member.guild.channels.fetch(goodbye.channel).catch(r=>{});
	if (!channel) return;
    if (!channel.permissionsFor(member.client.user.id).has(PermissionsBitField.Flags.ViewChannel)) return;
	if (!channel.permissionsFor(member.client.user.id).has(PermissionsBitField.Flags.SendMessages)) return;

    // Default messages
	goodbye.message ||= 'default';
    goodbye.banMessage ||= 'default';
    if (goodbye.message == 'default' || goodbye.banMessage == 'default') {
        const lang = await import(`../lang/${guildInfo.lang}.js`);
        if (goodbye.message == 'default') goodbye.message = lang.defaultValues.goodbye.message;
        if (goodbye.banMessage == 'default') goodbye.banMessage = lang.defaultValues.goodbye.banMessage;
    }

    // Check if it's a ban and send the appropirate message
	const message = await member.guild.bans.fetch(member.id).catch(r=>{}) ? goodbye.banMessage : goodbye.message;
	channel.send(
        message
        .replaceAll('{user}',member.user.tag)
        .replaceAll('{guild}',member.guild.name)
    ).catch(r=>{});
}