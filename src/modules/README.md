# src/modules
This directory contains functions that are from specific modules and not for general purpose.
Some are used multiple times, some are here just to keep the code more organized.