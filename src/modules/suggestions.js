/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

export default async function (message, modules) {
	let suggestions = modules.find((c) => c.name == 'suggestions');
	if (suggestions.enabled && message.channel.id == suggestions.channel)
	if (suggestions.reactToLinks && (message.content.includes('http://') || message.content.includes('https://')) || suggestions.reactToFiles && message.attachments.size>0) {
		await message.react(suggestions.approvalEmoji).catch(r=>{});
		await message.react(suggestions.disapprovalEmoji).catch(r=>{});
	}
}