/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { PermissionsBitField } from 'discord.js';
import getGuildInfo from '../utils/getGuildInfo.js';
import updateModules from '../utils/updateModules.js';

export async function addMessageXP(message, guildInfo) {

    // Check if module is enabled, the config is valid and the channel is not blacklisted
    const rank = guildInfo.modules.find((c) => c.name == 'rank');
    if (!rank.enabled) return;
    if (rank.xpBlacklistChannels.indexOf(message.channel.id) != -1) return;
    if (rank.xpPerMessage <= 0) return;

    // Find user or create it
    let user = rank.users.find(u => u.id == message.author.id);
    user ??= rank.users[rank.users.push({id: message.author.id, xp: 0, onCooldown: 0, onVC: {}})-1];

    // Return if user is on cooldown or has reached max xp
    if (user.xp >= Number.MAX_SAFE_INTEGER) return;
    if (user.onCooldown) return;

    // Add XP and calculate levels
    const currentLevel = Math.trunc((Math.sqrt(5)/5)*Math.sqrt(user.xp));
    user.xp+=rank.xpPerMessage;
    if (isNaN(parseInt(user.xp))) return;
    const newLevel = Math.trunc((Math.sqrt(5)/5)*Math.sqrt(user.xp));

    // Apply cooldown
    if (rank.messageCooldown > 0) {
        user.onCooldown = Date.now()
        setTimeout(() => {
            clearCooldown(user.id, message.guild);
        }, rank.messageCooldown*1000);
    }

    // Update database
    await updateModules(guildInfo.id, guildInfo.modules);

    // Announce levelup
    if ((currentLevel < newLevel) && rank.announceLevelUp)
    announceLevelUp(
        message.client,
        message.author,
        newLevel,
        rank.announceLevelUpChannel || message.channel.id,
        guildInfo.color,
        await import(`../lang/${guildInfo.lang}.js`)
    );

}

export async function checkRemainingCooldowns(guild, guildInfo) {
    const rank = guildInfo.modules.find(m=>m.name=='rank');
    for (const user of rank.users) {
        if (isNaN(user.onCooldown)) {
            user.onCooldown = 0;
            await updateModules(guild.id, guildInfo.modules);
        }
        if (!user.onVC) {
            user.onVC = {};
            await updateModules(guild.id, guildInfo.modules);
        }
        if (user.onCooldown) {
            const time = Date.now() - user.onCooldown;
            const member = await guild.members.fetch(user).catch(r=>{});
            if (time>rank.messageCooldown*1000 || !member) {
                await clearCooldown(user.id, guild);
            } else {
                setTimeout(() => {
                    clearCooldown(user.id, guild);
                }, rank.messageCooldown*1000-time);
            }

            // TODO: Make a script that gives users the XP they have earned so far in VC when the bot is stopped
            if (member?.voice?.channel) await joinVC(member.voice);
            else {
                user.onVC = {};
                await updateModules(guild.id, guildInfo.modules);
            }
        }
    }
}

export async function joinVC(state, guildInfo) {

    // Check if module is enabled, the config is valid and the channel is not blacklisted
    const rank = guildInfo.modules.find((c) => c.name == 'rank');
    if (!rank.enabled) return;
    if (rank.xpBlacklistChannels.indexOf(state.channel.id) != -1) return;
    if (rank.voiceChatCooldown <= 0 || rank.xpInVoiceChat <= 0) return;
    if (!state.member) return;

    // Find user or create it
    let user = rank.users.find(u => u.id == state.member.user.id);
    user ??= rank.users[rank.users.push({id: state.member.user.id, xp: 0, onCooldown: 0, onVC: {}})-1];
    if (user.xp >= Number.MAX_SAFE_INTEGER) return;

    // Save timestamp user joined VC
    user.onVC = {
        channel: state.channel.id,
        timestamp: Date.now()
    }
    await updateModules(guildInfo.id, guildInfo.modules);
}

export async function leaveVC(state, guildInfo) {

    // Check if module is enabled, the config is valid and the channel is not blacklisted
    const rank = guildInfo.modules.find((c) => c.name == 'rank');
    if (!rank.enabled) return;
    if (rank.xpBlacklistChannels.indexOf(state.channel.id) != -1) return;
    if (rank.voiceChatCooldown <= 0 || rank.xpInVoiceChat <= 0) return;
    if (!state.member) return;

    // Find user or create it
    let user = rank.users.find(u => u.id == state.member.user.id);
    user ??= rank.users[rank.users.push({id: state.member.user.id, xp: 0, onCooldown: 0, onVC: {}})-1];
    if (user.xp >= Number.MAX_SAFE_INTEGER) return;

    if (user.onVC.channel == state.channel.id) {
        const seconds = Math.trunc(Math.abs(new Date() - user.onVC.timestamp)/1000);
        if (seconds >= rank.voiceChatCooldown) {
            const currentLevel = Math.trunc((Math.sqrt(5)/5)*Math.sqrt(user.xp));
            // Add XP
            user.xp+=Math.trunc(seconds/rank.voiceChatCooldown)*rank.xpInVoiceChat;
            if (isNaN(parseInt(user.xp))) return;
            const newLevel = Math.trunc((Math.sqrt(5)/5)*Math.sqrt(user.xp));

            // Update database
            await updateModules(guildInfo.id, guildInfo.modules);

            // Announce levelup
            if ((currentLevel < newLevel) && rank.announceLevelUp)
            announceLevelUp(
                state.client,
                state.member.user,
                newLevel,
                rank.announceLevelUpChannel || state.channel.id,
                guildInfo.color,
                await import(`../lang/${guildInfo.lang}.js`)
            );
        }
    }
}

export async function announceLevelUp (client, user, level, channelID, color, lang) {
    if (!channelID) return;
    const channel = await client.channels.fetch(channelID).catch(r=>{});
    if (!channel) return;
    if (!channel.permissionsFor(client.user.id).has(PermissionsBitField.Flags.ViewChannel)) return;
    if (!channel.permissionsFor(client.user.id).has(PermissionsBitField.Flags.SendMessages)) return;
    channel.send({content: `${user}`, embeds:[{
        title: user.username,
        description: lang.levelup(level),
        color: color,
        thumbnail: { url: user.displayAvatarURL({ forceStatic: true, size: 512 }) }
    }]}).catch(r=>{});
}

export async function formatLeaderboard(users, guild, guildInfo, lang) {
    let lb = '';
    const highscores = users.sort((a, b) => (a.xp < b.xp) ? 1 : -1);
    for (const i in highscores.slice(0,10)) {
      lb+=`${getNumberEmoji(+i+1)} ► <@${highscores[i].id}>
            ${lang.level}: \`${Math.trunc((Math.sqrt(5)/5)*Math.sqrt(highscores[i].xp))}\`
            XP: \`${highscores[i].xp}\`\n`;
    }
    return {
      title: lang.leaderboard_title,
      description: lb,
      color: guildInfo.color,
      thumbnail: { url: guild.iconURL({size:1024}) }
    };
}

function getNumberEmoji(n) {
    switch (n) {
      case 1:
      return ':first_place:'
      case 2:
      return ':second_place:'
      case 3:
      return ':third_place:'
      case 4:
      return ':four:'
      case 5:
      return ':five:'
      case 6:
      return ':six:'
      case 7:
      return ':seven:'
      case 8:
      return ':eight:'
      case 9:
      return ':nine:'
      case 10:
      return ':keycap_ten:'
    }
}

async function clearCooldown(userID, guild) {
    const guildInfo = await getGuildInfo(guild);
    const rank = guildInfo.modules.find(c => c.name == 'rank');
    const user = rank.users.find(u => u.id == userID);
    user.onCooldown = 0;
    await updateModules(guildInfo.id, guildInfo.modules);
}
