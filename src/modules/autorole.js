/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { AttachmentBuilder } from 'discord.js';
import Captcha from "@haileybot/captcha-generator";
import getGuildInfo from '../utils/getGuildInfo.js';
import updateModules from '../utils/updateModules.js';
import reply from '../utils/reply.js';

export async function giveRole(member, lang, guildInfo) {
    let autorole = guildInfo.modules.find(c => c.name == 'autorole');
    if (!autorole.enabled) return;
    if (!autorole.requiresCaptcha) {
        // No CAPTCHA? Just give the role and done uwu
        if (!autorole.role) return;
        member.roles.add(autorole.role);
    } else {
        // Check if CAPTCHA config is valid first
        if (!autorole.captchaChannel) return;
        let channel = await member.guild.channels.fetch(autorole.captchaChannel).catch(r => { });
        if (!channel) return;

        // Give role before CAPTCHA (if any)
        if (autorole.roleBeforeCaptcha) member.roles.add(autorole.roleBeforeCaptcha).catch(r => { });

        // Generate CAPTCHA    
        let captcha = new Captcha();
        if (autorole.captchaDescription == 'default') autorole.captchaDescription = lang.defaultValues.autorole.captchaDescription;
        let m = await channel.send({
            content: `${member}`, embeds: [{
                title: 'Captcha',
                description: autorole.captchaDescription.replaceAll('{guild}', member.guild.name).replaceAll('{seconds}', autorole.captchaTimeout),
                color: guildInfo.color,
                image: { url: 'attachment://captcha.jpeg' }
            }], files: [new AttachmentBuilder(captcha.JPEGStream, { name: 'captcha.jpeg' })]
        });

        // Store captcha in the database in case the bot is restarted
        autorole.unverified[member.id] = {
            message: m.id,
            channel: m.channel.id,
            remainingAttempts: autorole.captchaMaxAttempts,
            timestamp: Date.now(),
            solution: captcha.value,
            roleToRemove: autorole.roleBeforeCaptcha,
            messagesToDelete: [m.id]
        };
        await updateModules(guildInfo.id, guildInfo.modules);

        setTimeout(async () => {
            captchaKick(member, lang);
        }, autorole.captchaTimeout * 1000);
    }
}

export async function checkCaptcha(message, guildInfo) {
    let autorole = guildInfo.modules.find(c => c.name == 'autorole');
    let captcha = autorole.unverified[message.member.id];
    if (captcha && captcha.channel == message.channel.id) {
        captcha.messagesToDelete.push(message.id);
        if (message.content.trim().toUpperCase() == captcha.solution) {
            // Correct CAPTCHA response
            if (captcha.roleToRemove) message.member.roles.remove(captcha.roleToRemove).catch(r => { });
            if (autorole.role) message.member.roles.add(autorole.role).catch(r => { });
            await deleteCaptchaMessages(captcha, message.guild);
            delete autorole.unverified[message.member.id];
        } else {
            if (!message.content) return;
            // Wrong CAPTCHA response
            let lang = await import(`../lang/${guildInfo.lang}.js`);
            captcha.remainingAttempts--;
            if (!captcha.remainingAttempts) {
                // Inform user that they have been kicked
                if (autorole.captchaKickMessage == 'default') autorole.captchaKickMessage = lang.defaultValues.autorole.captchaKickMessage;
                await message.author.send(autorole.captchaKickMessage
                    .replaceAll('{guild}', message.guild.name)
                    .replaceAll('{seconds}', autorole.captchaTimeout)
                ).catch(r => { console.log(r); });
                message.member.kick('Captcha failed').catch(r => { });
                await deleteCaptchaMessages(captcha, message.guild);
                delete autorole.unverified[message.author.id];
            } else {
                let r = await reply(message, lang.captcha_attempts_left(captcha.remainingAttempts), true);
                if (r) captcha.messagesToDelete.push(r.id);
            }
        }
        await updateModules(guildInfo.id, guildInfo.modules);
    }
}

export async function checkRemainingCaptchas(guild, guildInfo) {
    let autorole = guildInfo.modules.find(m => m.name == 'autorole');
    autorole.unverified ??= {};
    let lang = await import(`../lang/${guildInfo.lang}.js`);
    for (let id of Object.keys(autorole.unverified)) {
        let time = Date.now() - autorole.unverified[id].timestamp;
        let member = await guild.members.fetch(id).catch(r => { });
        if (!member) {
            // User left the server
            await deleteCaptchaMessages(autorole.unverified[id], guild);
            delete autorole.unverified[id];
            await updateModules(guildInfo.id, guildInfo.modules);
            continue;
        }
        if (time > autorole.captchaTimeout * 1000) {
            await captchaKick(member, lang, guildInfo);
        } else {
            setTimeout(async () => {
                captchaKick(member, lang, guildInfo);
            }, autorole.captchaTimeout * 1000 - time);
        }
    }
}

export async function deleteCaptchaMessages(captcha, guild) {
	let channel = await guild.channels.fetch(captcha.channel).catch(r=>{});
	if (channel) for (let id of captcha.messagesToDelete) {
		let m = await channel.messages.fetch(id).catch(r=>{});
		if (m) m.delete().catch(r=>{});
	}
}

export async function captchaKick(member, lang) {
    let guildInfo = await getGuildInfo(member.guild);
    let autorole = guildInfo.modules.find(c => c.name == 'autorole');
	let captcha = autorole.unverified[member.id]
	if (!captcha) return;
	await deleteCaptchaMessages(captcha, member.guild);
    if (autorole.captchaKickMessage == 'default') autorole.captchaKickMessage = lang.defaultValues.autorole.captchaKickMessage;
    await member.user.send(autorole.captchaKickMessage.replaceAll('{guild}',member.guild.name).replaceAll('{seconds}',autorole.captchaTimeout)).catch(r=>{});
    delete autorole.unverified[member.id];
    await updateModules(member.guild.id, guildInfo.modules);
    member.kick('Captcha timeout').catch(r=>{});
}

export async function memberLeft(guildInfo, member) {
    let autorole = guildInfo.modules.find(c => c.name == 'autorole');
    if (!autorole.unverified[member.id]) return;
    await deleteCaptchaMessages(autorole.unverified[member.id], member.guild);
    delete autorole.unverified[member.id];
    await updateModules(guildInfo.id, guildInfo.modules);
}