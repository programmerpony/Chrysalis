/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { EmbedBuilder } from "discord.js";

export async function sendDeletedMessage(message, guildInfo) {
    const logs = guildInfo.modules.find((c) => c.name == 'logs');
    if (logs.enabled && logs.logDeletedMessages && logs.channel) {

        const channel = await message.guild.channels.fetch(logs.channel).catch(r => { });
        if (!channel) return;
        const lang = await import(`../lang/${guildInfo.lang}.js`);

        const embed = new EmbedBuilder({
            title: lang.message_deleted,
            author: { name: message.author.tag, iconURL: message.author.displayAvatarURL() },
            color: guildInfo.color,
            fields: [
                {
                    name: lang.author,
                    value: `<@!${message.author.id}>`
                }
            ]
        });

        if (message.content)
            embed.addFields({ name: lang.message, value: message.content.slice(0, 1024) });

        if (message.attachments.size > 0)
            embed.addFields({ name: lang.attachments, value: message.attachments.map(a => a.name).join('\n').slice(0, 1024) });

        embed.addFields([
            { name: lang.message_id, value: message.id },
            { name: lang.channel, value: `${message.channel} [${lang.jump_to_moment}](${message.url})` }
        ]);

        channel.send({ embeds: [embed] }).catch(r => { });
    }
}

export async function sendEditedMessage(oldMessage, newMessage, guildInfo) {
    if (oldMessage.attachments.size == newMessage.attachments.size && oldMessage.content == newMessage.content) return;
    if (newMessage.author.id == newMessage.client.user.id) return;
    const logs = guildInfo.modules.find((c) => c.name == 'logs');
    if (logs.enabled && logs.logEditedMessages && logs.channel) {

        const channel = await newMessage.guild.channels.fetch(logs.channel).catch(r => { });
        if (!channel) return;
        const lang = await import(`../lang/${guildInfo.lang}.js`);

        const embed = new EmbedBuilder({
            title: lang.message_edited,
            author: {
                name: newMessage.author.tag,
                iconURL: newMessage.author.displayAvatarURL()
            },
            color: guildInfo.color,
            fields: [{ name: lang.author, value: `<@!${newMessage.author.id}>` }]
        });

        if (oldMessage.content != newMessage.content) {
            if (oldMessage.content) embed.addFields({ name: lang.old_message, value: oldMessage.content.slice(0, 1024) });
            if (newMessage.content) embed.addFields({ name: lang.new_message, value: newMessage.content.slice(0, 1024) });
        }

        if (oldMessage.attachments.size > 0 && oldMessage.attachments.size != newMessage.attachments.size) {
            embed.addFields({ name: lang.old_attachments, value: oldMessage.attachments.map(a => a.name).join('\n').slice(0, 1024) });
            if (newMessage.attachments.size > 0) embed.addFields({ name: lang.new_attachments, value: oldMessage.attachments.map(a => `[${a.name}](${a.url})`).join('\n').slice(0, 1024) });
        }

        embed.addFields([
            { name: lang.message_id, value: newMessage.id },
            { name: lang.channel, value: `${newMessage.channel} [${lang.jump_to_moment}](${newMessage.url})` }
        ]);

        channel.send({ embeds: [embed] }).catch(r => { });
    }
}