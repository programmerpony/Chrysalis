/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { PermissionsBitField } from "discord.js";

export default async function (message, guildInfo) {
    if (message.author.id == message.client.user.id) return;
	if (!message.channel.permissionsFor(message.client.user.id).has(PermissionsBitField.Flags.ManageMessages)) return;
	let bannedwords = guildInfo.modules.find((c) => c.name == 'bannedwords');
	if (!bannedwords.enabled) return false;
	if (message.member && message.member.permissions.has(PermissionsBitField.Flags.Administrator) && bannedwords.ignoreAdmins) return;
	let lang = await import(`../lang/${guildInfo.lang}.js`);
	for (let word of bannedwords.words) {
		if (message.content.toLowerCase().includes(word.toLowerCase())) {
			try {
				await message.delete();
				if (message.member && bannedwords.sendMessageOnDelete) await message.author.send(bannedwords.message == 'default' ? lang.defaultValues.bannedwords.message : bannedwords.message).catch(r=>{});
			} catch (e) {} finally {
				return true;
			}
		}
		if (message.attachments.size>0) {
			for (let word of bannedwords.words) {
				for (let attachment of message.attachments.values()) {
					if (attachment.name.toLowerCase().includes(word.toLowerCase())) {
						try {
							await message.delete();
							if (message.member && bannedwords.sendMessageOnDelete) await message.author.send(bannedwords.message == 'default' ? lang.defaultValues.bannedwords.message : bannedwords.message).catch(r=>{});
						} catch (e) {} finally {
							return true;
						}
					}
				}
			}
		}
	}
	return !message.member;
}