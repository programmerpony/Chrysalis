/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { Client, GatewayIntentBits, Collection, PermissionsBitField, OAuth2Scopes } from 'discord.js';
import presence from'./presence.js';
const client = new Client({
	failIfNotExists: false,
	presence: presence,
	intents: [
		GatewayIntentBits.MessageContent,
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.GuildMessageReactions,
		GatewayIntentBits.GuildMembers,
		GatewayIntentBits.GuildVoiceStates,
		GatewayIntentBits.GuildModeration,
		GatewayIntentBits.DirectMessages
	]
});

// Dependencies
import 'dotenv/config'
import path from 'path';
import fs from 'fs';
import { registerFont } from 'canvas';

// Utils
import getGuildInfo from './utils/getGuildInfo.js';
import reloadSlashCommands from './utils/reloadSlashCommands.js';

// Modules
import { giveRole, checkCaptcha, checkRemainingCaptchas, memberLeft } from './modules/autorole.js';
import bannedwords from './modules/bannedwords.js';
import boost from './modules/boost.js';
import goodbye from './modules/goodbye.js';
import { sendDeletedMessage, sendEditedMessage } from './modules/logs.js';
import { joinVC, leaveVC, addMessageXP, checkRemainingCooldowns } from './modules/rank.js';
import suggestions from './modules/suggestions.js';
import welcome from './modules/welcome.js';

// Fonts
registerFont('./src/fonts/Montserrat-Black.ttf',{ family: 'Montserrat', weight: 'Black' });
registerFont('./src/fonts/Montserrat-Medium.ttf',{ family: 'Montserrat', weight: 'Medium' });

client.on('ready', async () => {
	console.log(highlight(`Bot started as ${client.user.tag}`));
	await registerCommands();
	setInterval((() => { client.user.setPresence(presence); }), 1800000); // Refresh presence every half an hour so it doesn't vanish
	let totalMembers = 0;
	for (const guild of client.guilds.cache.values()) totalMembers += guild.memberCount;
	console.log(highlight(`${client.user.tag} is ready on ${client.guilds.cache.size} server${client.guilds.cache.size !== 1 ? 's' : ''} with a total of ${totalMembers} members!`));
});

client.on('guildCreate', async (guild) => {
	console.log(`Client joined guild ${guild.name} with ID ${guild.id}`);
	reloadSlashCommands(guild.client, guild, await getGuildInfo(guild));
});

client.on('messageCreate', async (message) => {

	if (!message.guild || message.author.bot || message.system) return; // Ignore DMs and bots

	const guildInfo = await getGuildInfo(message.guild);

	// Send help if bot is tagged
	if (message.content == `${client.user}`) await botInfo(message, guildInfo);

	// Blocked / Banned Words
	if (await bannedwords(message, guildInfo)) return;

	// Check if the message is an autorole captcha response
	checkCaptcha(message, guildInfo);

	// Run command or add XP
	runTextCommand(message, guildInfo);

	// Check the suggestions channel (if enabled)
	suggestions(message, guildInfo.modules);

});

client.on('messageUpdate', async (oldMessage, newMessage) => {
	if (newMessage.guild) {
		sendEditedMessage(oldMessage, newMessage, await getGuildInfo(newMessage.guild));
		bannedwords(newMessage, await getGuildInfo(newMessage.guild));
	}
});

client.on('messageDelete', async (message) => {
	if (message.author.id == client.user.id) return;
	if (message.guild) sendDeletedMessage(message, await getGuildInfo(message.guild));
});

client.on('guildMemberAdd', async (member) => {
	const guildInfo = await getGuildInfo(member.guild);
	const lang = await import(`./lang/${guildInfo.lang}.js`);
	await giveRole(member, lang, guildInfo);
	await welcome(guildInfo, member.guild, lang, member.user);
});

client.on('guildMemberRemove', async (member) => {
	const guildInfo = await getGuildInfo(member.guild);
	await goodbye(guildInfo, member);
	await memberLeft(guildInfo, member)
});

client.on('guildMemberUpdate', async (oldMember, newMember) => {
	// New booster
	if (!oldMember.premiumSince && newMember.premiumSince) {
		boost(newMember, await getGuildInfo(newMember.guild));
	}
	// Renewing boost
	if (oldMember.premiumSince && newMember.premiumSince && oldMember.premiumSinceTimestamp != newMember.premiumSinceTimestamp) {
		boost(newMember, await getGuildInfo(newMember.guild));
	}
});

client.on('voiceStateUpdate', async (oldState, newState) => {
	if (newState.member?.user?.bot) return;
    // If user wasn't in a VC or was in a different guild's VC
	if ((newState.channel?.id && !oldState.channel?.id) || (newState.channel?.guild?.id && oldState.channel?.guild?.id != newState.channel?.guild?.id)) {
		// User joins a voice channel
        await joinVC(newState, await getGuildInfo(newState.guild));
	}
    // If user has switched to a different guild's VC or isn't in a VC anymore
    if ((oldState.channel?.id && !newState.channel?.id) || (oldState.channel?.guild?.id && oldState.channel?.guild?.id != newState.channel?.guild?.id)) {
    	// User leaves a voice channel
	    await leaveVC(oldState, await getGuildInfo(oldState.guild));
	}
});

client.on('interactionCreate', async (i) => {
	if (i.isChatInputCommand()) return runSlashCommand(i);
	if (!i.isButton()) return;
	if (!i.guild) return i.deferUpdate();

	// Role menu
	if (i.customId.startsWith('role-')) {
		try { await i.deferUpdate(); } catch (e) { return; }
		const roleID = i.customId.replace('role-', '');
		await i.member.fetch(true);
		try {
			if (!i.member.roles.cache.get(roleID)) await i.member.roles.add(roleID);
			else await i.member.roles.remove(roleID);
		} catch (e) {
			const guildInfo = await getGuildInfo(i.guild);
			const lang = await import(`./lang/${guildInfo.lang}.js`);
			if (!i.guild.members.me.permissions.has(PermissionsBitField.Flags.ManageRoles)) await i.followUp({ content:lang.manage_roles_permission_required, ephemeral:true }).catch(r=>{});
			else if (i.guild.members.me.roles.highest.position < i.guild.roles.cache.get(roleID).position) await i.followUp({ content:lang.chrysalis_role_too_low, ephemeral:true }).catch(r=>{});
			else await i.followUp({ content: lang.roles_managed_by_integrations_cannot_be_manually_assigned, ephemeral: true }).catch(r=>{});
		}
	}

	// Delete inappropriate images
	if (i.customId.startsWith('report')) {
		try { await i.deferReply({ephemeral:true}); } catch(e) { return; }
		const args = i.customId.split('-');
		const reportURL = args[1];
		const commandMessage = args[2];
		const guildInfo = await getGuildInfo(i.guild);
		const lang = await import(`./lang/${guildInfo.lang}.js`);
		await i.editReply({embeds:[{
			title: lang.please_report,
			url: reportURL,
			color: guildInfo.color
		}]});
		try {
			i.message.delete();
			if (commandMessage) await i.channel.messages.fetch({ message: commandMessage, cache: false }).then(m => m.delete());
		} catch (e) {}
	}
});

client.login(process.env.DISCORD_TOKEN);

async function isRestricted(command, message, modules) {
	const cmdModule = modules.find((c) => c.name == command) || modules.find(c=>c.name==client.commands.get(command)?.dependsOn);
	if (!cmdModule.enabled) return 'disabled';
	if (cmdModule.restricted) return (cmdModule.allowedChannels.indexOf(message.channel.id) == -1);
	else return false;
}

async function runTextCommand(message, guildInfo) {

	if (!message.channel.permissionsFor(client.user.id).has(PermissionsBitField.Flags.SendMessages)) return;
	if (message.content.startsWith(guildInfo.prefix)) {
		const args = message.content.slice(guildInfo.prefix.length).split(/ +/g);
		const command = args.shift().toLowerCase();
		const cmd = client.commands.get(command) || client.commands.find((c) => c.alias.includes(command));
		if (cmd) {
			const lang = await import(`./lang/${guildInfo.lang}.js`);
			if (cmd.nsfw && !message.channel.nsfw) return message.author.send(lang.nsfw_only).catch(r=>{});
			let restricted = false;
			if (!cmd.admin) restricted = await isRestricted(cmd.name, message, guildInfo.modules);
			else if (!message.member.permissions.has(PermissionsBitField.Flags.Administrator)) return;
			if (restricted) return restricted === 'disabled' ? null : message.author.send(lang.wrong_channel).catch(r=>{/*User blocked Chrysalis*/});
			if (cmd.name!='clean') await message.channel.sendTyping().catch(r=>{});
			cmd.run(client, message, command, args, lang, guildInfo);
    }
	} else if (guildInfo.modules.find(m => m.name == 'rank').enabled) await addMessageXP(message, guildInfo);
}

async function runSlashCommand(i) {

	const guildInfo = await getGuildInfo(i.guild);
	if (!i.channel.permissionsFor(client.user.id).has(PermissionsBitField.Flags.SendMessages) || !i.channel.permissionsFor(client.user.id).has(PermissionsBitField.Flags.ViewChannel)) return;
	const lang = await import(`./lang/${guildInfo.lang}.js`);

	const command = i.commandName;
	const args = i.options.data.map(d => d.value);
	const cmd = client.commands.get(command) || client.commands.find((c) => c.alias.includes(command));
	if (cmd) {
		if (cmd.nsfw && !i.channel.nsfw) return i.reply({content:lang.nsfw_only,ephemeral:true});
		let restricted = false;
		if (!cmd.admin) restricted = await isRestricted(command, i, guildInfo.modules);
		else if (!i.member.permissions.has(PermissionsBitField.Flags.Administrator)) return;
		if (restricted) return i.reply({content:lang.wrong_channel,ephemeral:true}).catch(r=>{});
		else {
			if (!cmd.modal) try { await i.deferReply({ephemeral:cmd.ephemeral}); } catch (e) { return; }
			cmd.run(client, i, command, args, lang, guildInfo);
		}
	}
}

async function registerCommands() {
	client.commands = new Collection();
	const commands = fs.readdirSync(path.resolve(import.meta.dirname, 'commands')).filter((f) => f.endsWith('.js'));
	for (const jsfile of commands) {
		const commandfile = await import(`./commands/${jsfile}`);
		client.commands.set(commandfile.name, commandfile);
		console.log(`${jsfile} loaded`);
	}
	for (const guild of client.guilds.cache.values()) {
		const guildInfo = await getGuildInfo(guild);
		await reloadSlashCommands(client, guild, guildInfo);
		await checkRemainingCaptchas(guild, guildInfo);
		await checkRemainingCooldowns(guild, guildInfo);
	}
}

async function botInfo(message, guildInfo) {
	if (!message.channel.permissionsFor(client.user.id).has(PermissionsBitField.Flags.SendMessages)) return;
	const lang = await import(`./lang/${guildInfo.lang}.js`);
	const invite = client.generateInvite({
		permissions: [
			PermissionsBitField.Flags.Administrator
		],
		scopes: [
			OAuth2Scopes.Bot,
			OAuth2Scopes.ApplicationsCommands
		]
	});
	message.channel.send({embeds:[{
		title: client.user.username,
		description: `[${lang.invite_the_bot}](${invite}) | [${lang.website}](https://chrysalis.programmerpony.com) | [${lang.support_server}](https://discord.gg/YMAvTJyTzT)`,
		thumbnail: { url: client.user.displayAvatarURL() },
		color: guildInfo.color,
		fields: [
			{
				name: `💻 ${lang.source_code}`,
				value: `[Codeberg (Forgejo)](https://codeberg.org/programmerpony/Chrysalis)`,
				inline: true
			},
			{
				name: `💞 ${lang.support_the_project}`,
				value: '[Liberapay](https://liberapay.com/programmerpony/) | [Ko-Fi](https://ko-fi.com/programmerpony)',
				inline: true
			}
		],
		footer: {text: lang.the_current_prefix_for_this_server_is(guildInfo.prefix)}
	}]}).catch(r=>{});
}

function highlight(s) { return `\u001b[47m\u001b[30m${s}\u001b[49m\u001b[39m`; }
