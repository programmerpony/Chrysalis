/*

 Copyright (C) 2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

export default async (url) => {
    try {
        let valid_url = new URL(url);
        if (valid_url.hostname !== 'cdn.discordapp.com' && valid_url.hostname !== 'media.discordapp.net') return valid_url.href;
        let check = await fetch(valid_url.href);
        if (check.status == 200) return valid_url.href;
        let res = await fetch('https://discord.com/api/v9/attachments/refresh-urls', {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Authorization': `Bot ${process.env.DISCORD_TOKEN}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ attachment_urls: [valid_url.href] })
        });
        if (res.status !== 200) return valid_url.href;
        let json = await res.json();
        return json?.refreshed_urls?.[0]?.refreshed;
    } catch (e) {
        return url;
    }
}
