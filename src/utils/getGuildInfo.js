/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { resolveColor } from 'discord.js';
import defaultModules from '../defaultModules.js';
import connectToDatabase from './connectToDatabase.js';
import reloadSlashCommands from './reloadSlashCommands.js';

export default async function getGuildInfo(guild) {
	const db = await connectToDatabase();
	const guilds = db.db('chrysalis').collection('guilds');
	const guildInfo = await guilds.findOne({id: guild.id});
	if (!guildInfo) return await createGuild(guild);
	// Remove null modules
	guildInfo.modules = guildInfo.modules.filter(m=>m!==null);
	// Add missing modules
	for (const dm of defaultModules) if (!guildInfo.modules.find((m) => m.name == dm.name)) guildInfo.modules.push(dm);
	for (const m of guildInfo.modules) {
		const dm = defaultModules.find((dm) => dm.name == m.name);
		// Remove leftover modules
		if (!dm) {
			delete guildInfo.modules[guildInfo.modules.indexOf(m)];
			guildInfo.modules = guildInfo.modules.filter(m=>m!==null);
			continue;
		}
		const model = JSON.parse(JSON.stringify(dm));
		// Add missing properties and remove leftover properties
		for (const p in m) if (p in model) model[p] = m[p];
		guildInfo.modules[guildInfo.modules.indexOf(m)] = JSON.parse(JSON.stringify(model));
	}

	// MIGRATION: Move unverified to autorole module
	if (guildInfo.hasOwnProperty('unverified')) {
		if (guildInfo.unverified) guildInfo.modules.find(m=>m.name=='autorole').unverified = JSON.parse(JSON.stringify(guildInfo.unverified));
		await guilds.updateOne({id: guild.id},{ $unset: { unverified: '' }});
	}

	await guilds.updateOne({id: guild.id},{ $set: {
		modules: guildInfo.modules,
		color: resolveColor(guildInfo.color)
	}});
	db.close();
	return guildInfo;
}

async function createGuild(guild) {
	const db = await connectToDatabase();
	const guilds = db.db('chrysalis').collection('guilds');
	if (!(await guilds.findOne({id: guild.id}))) {
		await guilds.insertOne({
			id: guild.id,
			lang: guild.client.commands.get('lang').validLangs.indexOf(guild.preferredLocale.slice(0,2)) >= 0 ? guild.preferredLocale.slice(0,2) : 'en',
			prefix: 'c!',
			color: 0x3e804c,
			modules: defaultModules
		});
		console.log(`Created guild ${guild.name} with ID ${guild.id}`);
	}
	db.close();
	const guildInfo = await getGuildInfo(guild);
	await reloadSlashCommands(guild.client, guild, guildInfo);
	return guildInfo;
}