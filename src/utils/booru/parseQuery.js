/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

export default async (message, args) => {
  if (args.length === 0) return ''; // Nothing has to be processed
  let query = message.author ? args.join(' ') : args[0];
  
  query = query.replace(/\s*,/g, ',');  // Remove spaces before commas
  query = query.replace(/,\s*/g, ',');  // Remove spaces after commas
  query = query.replace(/,+/g, ',');    // Remove consecutive commas
  
  query = query.replace(/^,/, '');      // Remove leading comma
  query = query.replace(/,$/, '');      // Remove trailing comma

  return query;
}
