/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

export default async (client, guild, guildInfo) => {
	const lang = await import(`../lang/${guildInfo.lang}.js`);
	const applicationCommands = [];

	for (const command of client.commands.values()) {
		if (command.dependsOn) {
			if (!guildInfo.modules.find((c) => c.name == command.dependsOn)?.enabled) continue;
		} else {
			const m = guildInfo.modules.find((c) => c.name == command.name);
			if (m ? !m.enabled : !command.admin) continue;
		}
    	const cmdtxt = lang.commands.user.find((c) => c.name == command.name) || lang.commands.admin.find((c) => c.name == command.name);
    	if (!cmdtxt) continue;

    	const clonedCmdtxt = structuredClone(cmdtxt);
		if (command.admin) {
			clonedCmdtxt.default_member_permissions = '0';
			clonedCmdtxt.description = '🛠 '.concat(clonedCmdtxt.description);
		}

    	applicationCommands.push(clonedCmdtxt);
  	}

 	try {
    	await guild.commands.set(applicationCommands);
    	console.log(`Successfully (re)loaded slash commands on ${guild.name}`)
  	} catch (ignored) {
    	console.log(`Can't load slash commands on ${guild.name}`)
  	}
}
