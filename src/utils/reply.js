/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

export default async (message, response, ping) => {
    if (!message.author)  {
        await message.deferReply(response).catch(r=>{});
        return await message.editReply(response).catch(r=>{});
    }
    if (!ping) {
        return await message.channel.send(response).catch(r=>{});
    }
    try { return await message.reply(response); }
    catch (ignored) {
        if (response.content) {
            response.content = `${message.member} ${response.content}`;
        }
        return await message.channel.send(response).catch(r=>{});
    }
}
