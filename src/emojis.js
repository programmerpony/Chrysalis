/*

 Copyright (C) 2022-2024 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/*

You can download these emojis by copying their IDs and pasting
them on Chrysalis' emoji command. You can then upload them to
the Emoji section of your app on Discord's Developer Portal.
Once you've uploaded them, click on the Copy Markdown button
of each one and paste it on the according constant.

*/

export const NitroBoost = '<a:NitroBoost:939892339369971832>';
export const PinkiePieYes = '<:PinkieYes:945776821121728522>';
export const PinkiePieNo = '<:PinkieNo:945776828365295616>';